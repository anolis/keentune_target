# Copyright (c) 2021-2023 Alibaba Cloud Computing Ltd.
# SPDX-License-Identifier: MulanPSL-2.0
# pylint: disable=C,redefined-outer-name,unspecified-encoding,import-error,logging-format-interpolation,broad-except,no-self-use,protected-access,abstract-method,consider-using-with,too-many-instance-attributes,too-many-arguments,eval-used,fixme,attribute-defined-outside-init,anomalous-backslash-in-string

from setuptools import setup, find_packages

setup(
    name             = "keentune-target",
    version          = "3.1.0",
    description      = "KeenTune target unit",
    url              = "https://gitee.com/anolis/keentune_target",
    license          = "MulanPSLv2",
    packages         = find_packages(exclude=["test"]),
    package_data     = {'target': ['target.conf']},
    python_requires  = '>=3.6',
    long_description = "",

    classifiers = [
        "Environment:: KeenTune",
        "IntendedAudience :: Information Technology",
        "IntendedAudience :: System Administrators",
        "License :: OSI Approved :: MulanPSLv2",
        "Operating System :: POSIX :: Linux",
        "Intended Audience :: Developers",
        "Programming Language :: Python :: 3.6",
        "ProgrammingLanguage :: Python"
    ],

    data_files  = [
        ("/etc/keentune/target", ["agent/target.conf"]),
        ("/etc/keentune/target", ["agent/configfile.conf"]),
        ("/etc/keentune/target/scripts", ["agent/scripts/erdma.sh"]),
        ("/var/keentune/lib", []),
        ("/var/log/keentune",[]),
        ("/var/keentune/target/backup",[]),
    ],

    entry_points = {
        'console_scripts': ['keentune-target=agent.agent:main']
    }
)
