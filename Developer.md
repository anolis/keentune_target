# Developer Guide
本文档介绍如何在keentune-target中定义一种新的参数设置方式，并将他作为keentune-target的一部分。 


## 一、参数处理模块的动态加载机制  

我们在 keentune-target 中实现两种参数处理模块，分别称为 domain 和 feature , 对应的文件放在 agent/domain 和 agent/feature 文件夹中. 一般来说 domain 中定义一组参数的设置方式，例如 sysctl.py 中定义所有使用 sysctl 来进行设置的参数的处理逻辑; feature 中定义某一个参数的设置方式, 例如中断绑核方式.

我们也经常用 feature 来定义某个特性的打开和关闭, 例如eRDMA, 这是由于在 profile 文件中，这个特性被抽象为 eRDMA 这一个参数，参数值为 on/off 代表特性打开或者关闭。总得来说选择使用 domain 还是 feature 来定义主要取决于是否有多个参数有相同的处理逻辑，如果有相同的处理逻辑则应该使用domain来定义参数的设置行为.

keentune-target 使用动态加载的方式来加载不同的 domain/feature 模块，即只有当这个 domain/feature 模块被使用到的时候，domain/feature 类中的代码才被真正运行. 但是模块文件本身会在 keentune-target 启动的时候被导入，任何依赖或其他错误都会导致这个模块加载失败而无法使用，这些信息在 keentune-target 启动的时候就可以看到.

feature 在实现的时候也会声明了其隶属于一个 domain, 例如 RPS 这个参数(feature)声明其属于 net 这个 domain, 这只影响其在 profile 文件中的写法和位置，且并不会影响同时存在一个名为 net 的 domain 来处理其他网络参数的设置，只是 RPS 这个参数只会调用 net:PRS 这个 feature 的处理流程。


## 二、Domain的函数定义和约定

### 2.1. 继承和实现抽象函数
我们可以通过继承 BaseDomain 基类来实现新的domain文件，首先在 agent/domain 文件夹下创建一个新的python文件, 在文件中定义新的 domain 类并即成 BaseDomain 类, 定义**__domain__类变量**和至少实现三个函数: __init__(), set_value() 和 get_value(). 

__init__() 函数需要检查当前 domain 的可用性, 例如当前环境是否适用于当前特性, 所需依赖和文件是否存在, 所需服务是否已经启动, 并尝试初始化所有需要的服务和组件, 如果有异常发生或 domain 并不适用于当前环境, 可以直接抛出异常表明 domain 无法使用, **此时所有与当前 domain 相关的操作均会失败, 但并不会影响其他 domain 或者 feature 的运行**.

set_value() 和 get_value() 实现具体的参数读/设置逻辑, **函数每次只处理一个参数的读/设置**，同时也可以直接抛出异常表明此参数的读/设置失败. **当读失败时，由于此参数无法被备份，参数设置时会被跳过，当写失败时，可能会导致参数设置和参数回退报告Warning.**

```python
from agent.domain.base import BaseDomain

class MyDomain(BaseDomain):
    __domain__ = "my_domain"

    def __init__(self):
        """ init domain

        Args: 
            -
        Return: 
            -
        Raise:
            Exception: init failed 
        """
        super().__init__(domain_name = self.__domain__)

    def set_value(self, param_name:str, param_value):
        """ Setting param value

        Arg:
            param_name: parameter name to be set
            param_value: parameter value or schema_name to be set

        Return:
            -

        Raise:
            Exception: setting failed 
        """
        pass

    def get_value(self, param_name:str):
        """ Read param value

        Arg:
            param_name: parameter name to be read
        
        Return
            param_value: parameter value
        
        Raise:
            Exception: setting failed 
        """
        pass
```

#### 约定: 所有 get_value() 可能读取的参数值都要能被 set_value() 函数设置回去, 即下面示例的单元测试用例要能通过
``` python
from agent.domain.mydomain import MyDomain

handler = MyDomain()
value = handler.get_value(param_name = "my_param")
handler.set_value(param_name = "my_param", param_value = value)
```

## 三、Feature的函数定义和约定
我们可以通过继承 Feature 基类来实现新的 feature 文件，首先在 agent/feature 文件夹下创建一个新的python文件, 在文件中定义新的 feature 类并即成 Feature 类, 定义**__domain__和__feature__类变量**和至少实现三个函数: __init__(), set_value() 和 get_value(). 

```python
class MyFeature(Feature):
    __domain__  = "affiliated_domain"
    __feature__ = "my_feature"

    def __init__(self) -> None:
        super().__init__(self.__domain__, self.__feature__)
    
    def set_value(self, value):
        pass

    def get_value(self):
        pass
```
各个函数的含义与Domain基本一致, 唯一的区别是不再需要定义param_name了, 这是因为feature的每个类只处理一个参数, __feature__变量即参数名

## 四、其他
### 4.1. 脚本文件
如果在 domain/feature 类中使用shell脚本, 可以将shell脚本放到 agent/scripts 文件夹中, 这些脚本会在 keentune-target 安装的时候放到路径 /etc/keentune/target/scripts 下, 在代码中可以通过变量 Config.SCRIPTS_PATH 找到脚本文件的安装位置  
``` python
from agent.common.config import Config

script_path = os.path.join(Config.SCRIPTS_PATH, "my_script.sh")
```


