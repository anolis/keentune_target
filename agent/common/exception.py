# Copyright (c) 2021-2023 Alibaba Cloud Computing Ltd.
# SPDX-License-Identifier: MulanPSL-2.0
# pylint: disable=C,redefined-outer-name,unspecified-encoding,import-error,logging-format-interpolation,broad-except,no-self-use,protected-access,abstract-method,consider-using-with,too-many-instance-attributes,too-many-arguments,eval-used,fixme,attribute-defined-outside-init

import logging

logger = logging.getLogger('common')

class MacroParseException(Exception):
    def __init__(self, e) -> None:
        self.str = "{}".format(e)
        logger.error(self.str)
         
    def __str__(self) -> str:
        return self.str
    

class DomainBasicException(Exception):
    def __init__(self, msg, domain="", feature=""):
        self.msg = msg
        self.domain  = domain
        self.feature = feature
        if domain != "" and feature != "":
            self.str = "'{}:{}' exception: {}".format(self.domain, self.feature, self.msg)
        elif domain != "":
            self.str = "'{}' exception: {}".format(self.domain, self.msg)
        else:
            self.str = "{}".format(self.msg)
        
        logger.error(self.str)

    def __str__(self) -> str:
        return self.str


class InitException(DomainBasicException):
    def __init__(self, msg, domain="", feature=""):
        self.msg = msg
        self.domain  = domain
        self.feature = feature
        if domain != "" and feature != "":
            self.str = "'{}:{}' init exception: {}".format(self.domain, self.feature, self.msg)
        elif domain != "":
            self.str = "'{}' init exception: {}".format(self.domain, self.msg)
        else:
            self.str = "init exception: {}".format(self.msg)
        
        logger.warning(self.str)