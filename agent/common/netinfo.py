# Copyright (c) 2021-2023 Alibaba Cloud Computing Ltd.
# SPDX-License-Identifier: MulanPSL-2.0
# pylint: disable=C,redefined-outer-name,unspecified-encoding,import-error,logging-format-interpolation,broad-except,no-self-use,protected-access,abstract-method,consider-using-with,too-many-instance-attributes,too-many-arguments,eval-used,fixme,attribute-defined-outside-init,too-few-public-methods,too-many-branches

import os
import re
import logging

from agent.common.system import sysCommand, Singleton

logger = logging.getLogger('common')

@Singleton
class NetInfo():
    def __init__(self):
        self._load_pci_dev()

        for virtio_dev, dev_name in self.pci_dev_list:
            try:
                self._load_pci_id(dev_name)
                self._load_queue_list(dev_name)
                self._load_interrupts(virtio_dev, dev_name)

            except Exception:
                continue
            
            else:
                self.dev_name = dev_name
                break
        
        assert self.dev_name

        self.cpu_num   = int(sysCommand("cat /proc/cpuinfo| grep 'physical id'| sort| uniq| wc -l"))
        self.processor = int(sysCommand("cat /proc/cpuinfo| grep 'processor'| wc -l"))
        self._load_numa_node_core_range()
        
        if self.processor != self.numa_node_core_range[1] - self.numa_node_core_range[0] + 1:
            logger.warning("cpu number({}) unmatch with numa node number({})!".format(
                self.processor,
                self.numa_node_core_range[1] - self.numa_node_core_range[0] + 1
            ))


    def _load_pci_dev(self):
        self.pci_dev_list = []
        for dev in sysCommand("ls /sys/class/net").split("\n"):
            _path = os.path.join("/sys/class/net", dev)
            if not os.path.isdir(_path):
                continue
            
            try:
                _link_path = os.readlink(_path)
                if re.search(r"pci\d+", _link_path):
                    if re.search(r"virtio\d", _link_path):
                        self.pci_dev_list.append((re.search(r"virtio\d", _link_path).group(0), dev))
                    else:
                        self.pci_dev_list.append((dev, dev))
                        
            except OSError:
                continue
        

    def _load_pci_id(self, dev_name):
        pci_info = sysCommand("ethtool -i {}".format(dev_name))
        self.pci_id = re.search(r"bus-info: (.*)\n", pci_info).group(1)
        assert self.pci_id != ''


    def _load_queue_list(self, dev_name):
        queue_info = sysCommand("ls /sys/class/net/{}/queues/".format(dev_name))
        tx_pattern, rx_pattern = re.compile(r"tx-\d+"), re.compile(r"rx-\d+")
        if not re.search(tx_pattern, queue_info) or not re.search(rx_pattern, queue_info):
            raise Exception("can not find tx/rx queue in /sys/class/net/{}/queues/".format(dev_name))
        
        self.tx_queue = sorted(re.findall(tx_pattern, queue_info), key = lambda x: int(re.search(r"(\d+)", x).group(1)))
        self.rx_queue = sorted(re.findall(rx_pattern, queue_info), key = lambda x: int(re.search(r"(\d+)", x).group(1)))


    def __parse_interrupts_code(self, interrupts):
        result = []
        for i in interrupts:
            interrupts_id = re.search(r"^(\d+):", i).group(1)
            result.append(interrupts_id)
        return result


    def _load_interrupts(self, virtio_dev, dev_name):
        content = [i.strip() for i in sysCommand("cat /proc/interrupts").split('\n')]
        
        interrupts_list = [
            i for i in content if re.search("{dev_name}-input|{dev_name}-output".format(
            dev_name = dev_name),i)]
        
        if interrupts_list.__len__() == 0:
            interrupts_list = [
                i for i in content if re.search("{virtio_dev}-input|{virtio_dev}-output".format(
                virtio_dev = virtio_dev),i)]
        
        if interrupts_list.__len__() == 0:
            interrupts_list = [i for i in content if re.search("{}".format(dev_name),i)]

        assert interrupts_list.__len__() != 0
        logger.debug("parse interrupts_list: \n{}".format("\n".join(interrupts_list)))
        self.interrupts = self.__parse_interrupts_code(interrupts_list)
        assert self.interrupts.__len__() > 0

        # Split net interrupts to input interrupts and output interrupts
        self.input_interrupts_list = self.interrupts[0::2]
        self.output_interrupts_list = self.interrupts[1::2]

        logger.debug("input interrupts: {}".format(self.input_interrupts_list))
        logger.debug("output interrupts: {}".format(self.output_interrupts_list))


    def _load_numa_node_core_range(self):
        numa_message = sysCommand("lspci -vvvs {}".format(self.pci_id))
        if re.search(r"NUMA node: (\d)\n", numa_message):
            numa_node_num = re.search(r"NUMA node: (\d)\n", numa_message).group(1)
        else:
            numa_node_num = 0

        cpu_message = sysCommand("lscpu")
        if self.processor == 1:
            pattern_en = re.compile(r"NUMA 节点{} CPU：\s*(\d+)".format(numa_node_num))
            pattern = re.compile(r"NUMA node{} CPU\(s\):\s*(\d+)".format(numa_node_num))
            if re.search(pattern, cpu_message):
                numa_node_core_range = [re.search(pattern, cpu_message).group(1),] * 2
            elif re.search(pattern_en, cpu_message):
                numa_node_core_range = [re.search(pattern_en, cpu_message).group(1),] * 2
            else:
                logger.error("can not find numa_node_core_range in process = 1")
                raise Exception("can not find numa_node_core_range in process = 1")
                
        elif self.processor == 2:
            pattern_en = re.compile(r"NUMA 节点{} CPU：\s*(\d+,\d+)".format(numa_node_num))
            pattern = re.compile(r"NUMA node{} CPU\(s\):\s*(\d+,\d+)".format(numa_node_num))
            if re.search(pattern, cpu_message):
                numa_node_core_range = re.search(pattern, cpu_message).group(1).split(',')
            elif re.search(pattern_en, cpu_message):
                numa_node_core_range = re.search(pattern_en, cpu_message).group(1).split(',')
            else:
                logger.error("can not find numa_node_core_range in process = 2")
                raise Exception("can not find numa_node_core_range in process = 2")
            
        else:
            pattern_en = re.compile(r"NUMA 节点{} CPU：\s*(\d+-\d+)".format(numa_node_num))
            pattern = re.compile(r"NUMA node{} CPU\(s\):\s*(\d+-\d+)".format(numa_node_num))
            if re.search(pattern, cpu_message):
                numa_node_core_range = re.search(pattern, cpu_message).group(1).split('-')
            elif re.search(pattern_en, cpu_message):
                numa_node_core_range = re.search(pattern_en, cpu_message).group(1).split('-')
            else:
                logger.error("can not find numa_node_core_range in process = {}".format(self.processor))
                raise Exception("can not find numa_node_core_range in process = {}".format(self.processor))
        
        self.numa_node_core_range = [int(i) for i in numa_node_core_range]
        logger.debug("numa node core range: {}".format(self.numa_node_core_range))
