# Copyright (c) 2021-2023 Alibaba Cloud Computing Ltd.
# SPDX-License-Identifier: MulanPSL-2.0
# pylint: disable=C,redefined-outer-name,unspecified-encoding,import-error,logging-format-interpolation,broad-except,no-self-use,protected-access,abstract-method,consider-using-with,too-many-instance-attributes,too-many-arguments,eval-used,fixme,attribute-defined-outside-init,too-few-public-methods
import subprocess
import logging

logger = logging.getLogger('common')

class Singleton:
    def __init__(self, cls):
        self._cls = cls
        self._instance = {}
    
    def __call__(self):
        if self._cls not in self._instance:
            self._instance[self._cls] = self._cls()
        return self._instance[self._cls]


def sysCommand(command: str, cwd: str = "./", log: bool = False):
    '''Run system command with subprocess.run and return result
    '''
    result = subprocess.run(
        command,
        shell=True,
        close_fds=True,
        cwd=cwd,
        stderr=subprocess.PIPE,
        stdout=subprocess.PIPE,
        check=True
    )
    out = result.stdout.decode('UTF-8', 'strict').strip()
    if log:
        logger.info("{}: {}".format(command, out))
    else:
        logger.debug("{}: {}".format(command, out))
    return out  

