import re
import logging

from agent.common.system import sysCommand
from agent.common.exception import MacroParseException

logger = logging.getLogger('common')

def _innodb_log_file_size():
    cpu_core = int(sysCommand("cat /proc/cpuinfo | grep process | wc -l"))

    if cpu_core == 2:
        return "1500M"
    if cpu_core == 4:
        return "2048M"
    if cpu_core == 8:
        return "4096M"
    if cpu_core == 16:
        return "8192M"
    if cpu_core == 32:
        return "10240M"
    if cpu_core == 64:
        return "20480M"
    return "2048M"


def _cpu_model():
    cpu_info = sysCommand("cat /proc/cpuinfo")

    if re.search(r"vendor_id\s*:(.*)", cpu_info):
        vendor_id = re.search(r"vendor_id\s*:(.*)", cpu_info).group(1).strip()
        if vendor_id == "AuthenticAMD":
            return "amd"
        
        if vendor_id == "GenuineIntel":
            return "intel"

        return "unknow"
    else:
        return "yitian"


def _virt():
    try:
        sysCommand("systemd-detect-virt").strip()
        return "virt"
    
    except Exception:
        return "none"


MACROES = {
    "uname_arch" : lambda : sysCommand("arch").strip(),
    "cpu_core"   : lambda : sysCommand("cat /proc/cpuinfo | grep process | wc -l").strip(),
    "mem_total"  : lambda : sysCommand("free -g| grep 'Mem' | awk '{print $2}'").strip(),
    "mem_free"   : lambda : sysCommand("free -g| grep 'Mem' | awk '{print $4}'").strip(),
    "os_release" : lambda : sysCommand("cat /etc/os-release | grep ^NAME=").strip(),
    "virt"       : _virt,
    "cpu_model"  : _cpu_model,
    "innodb_log_file_size": _innodb_log_file_size
}

macro_pattern = r"#!.*?#"

def _evalMacro(macro):
    """ parse macro to value

    Args:
        expression (str): macro expression
    
    e.g.
        #!UNAME_ARCH#, #!CPU_CORE#, #!MEM_TOTAL#
    
    Returns:
        str: macro value
    """
    macro = macro.lower()[2:-1].strip()
    res = MACROES[macro]().strip()
    return res


def _replaceMacro(expression):
    _expression = expression
    for macro in re.findall(macro_pattern, expression):
        macro_value = _evalMacro(macro)
        _expression = re.sub(macro, macro_value, _expression)
        logger.info("replace macro {} -> {}, expression = {}".format(macro, macro_value, _expression))
    return _expression


def _evalCondition(subcondition):
    condition_operator = [">=", "<=", ">", "<", "=", "==", "!="]

    _condition = []
    for word in [i.strip() for i in re.split(r"({})".format("|".join(condition_operator)), subcondition,  maxsplit = 1)]:
        if word == "=":
            _condition.append("==")

        elif word in condition_operator or word.isdigit():
            _condition.append(word)

        elif word != "":
            _condition.append("'{}'".format(word))
    
    condition = " ".join(_condition)
    res = eval(condition)
    logger.info("eval condition {} to {}".format(condition, res))
    return str(res)


def parseExpression(expression):
    """ Parse numerical expression and return digital value

    Args:
        expression (str): numerical expression

    Raises:
        MacroParseException: can not parse expression

    Returns:
        int: numerical result
    
    e.g.
        (#!MEM_TOTAL# + 1) * 1073741824 / 2
    """
    if type(expression) is not str:
        return expression

    if expression.startswith('"') and expression.endswith('"'):
        expression = expression[1:-1]

    if not re.search(macro_pattern, expression):
        return expression
    
    _expression = _replaceMacro(expression)
    
    try:
        express_value = eval(_expression)
        logger.info("parse expression '{}' to '{} = {}'".format(expression, _expression, express_value))
        return int(express_value)
    except Exception:
        return _expression


def parseCondition(condition):
    """ Parse conditional expression and return boolean value

    Args:
        condition (str): conditional expression

    Returns:
        bool: boolean result
    
    e.g.
        #!CPU_CORE# = 8 & #!MEM_TOTAL# >= 29 & #!MEM_TOTAL# < 32
    """
    if type(condition) is not str:
        return False
    
    if condition.startswith('"') and condition.endswith('"'):
        condition = condition[1:-1]

    condition_splits = ["&", "|"]
    condition_result = []
    logger.info("parse condition {}".format(condition))

    try:
        _condition = _replaceMacro(condition)
        for subcondition in re.split(r"([{}])".format(".".join(condition_splits)), _condition):
            if subcondition in condition_splits:
                condition_result.append(subcondition)
            else:
                sub_res = _evalCondition(subcondition)
                condition_result.append(sub_res)
        
        condition_result = eval(" ".join(condition_result))
        return condition_result
    
    except Exception as e:
        raise MacroParseException(e)


if __name__ == "__main__":
    print(parseCondition("#!CPU_CORE# = 32 & #!MEM_TOTAL# >= 29 & #!MEM_TOTAL# < 132"))
    print(parseCondition("#!uname_arch#= aarch64"))
    print(parseCondition("#!uname_arch#= x86_64"))