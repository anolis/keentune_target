# Copyright (c) 2021-2023 Alibaba Cloud Computing Ltd.
# SPDX-License-Identifier: MulanPSL-2.0
# pylint: disable=C,redefined-outer-name,unspecified-encoding,import-error,logging-format-interpolation,broad-except,no-self-use,protected-access,abstract-method,consider-using-with,too-many-instance-attributes,too-many-arguments,eval-used,fixme,attribute-defined-outside-init,too-few-public-methods

import os
import re
import math


def padding_code(code, total_cpus: int, used_cpus: int = -1):
    total_bit_num = math.ceil(total_cpus/4)
    if used_cpus > 0:
        used_bit_num = math.ceil(used_cpus / 4)
    else:
        used_bit_num = total_bit_num
    
    # padding '0' before code
    code = "0" * (used_bit_num - len(code)) + code
    code = code + "0" * (total_bit_num - len(code))

    # split code with ','
    split_code = []
    for count, i in enumerate(code[::-1]):
        split_code.append(i)
        if (count + 1) % 8 == 0 and count != len(code) - 1:
            split_code.append(",")
    
    return "".join(split_code[::-1])


def glob(path):
    paths = _glob([path])
    return [p for p in paths if os.path.exists(p)]


def _glob(path_list:list):
    """ replace '*' wildcard in path and return all matched path

    """
    def _splitByWildcard(path):
        prefix   = '/'
        wildcard = ''
        suffix   = ''
        
        for _p in path.split('/'):
            if wildcard != '':
                suffix = os.path.join(suffix, _p)
            elif '*' in _p:
                wildcard = _p
            else:
                prefix = os.path.join(prefix, _p)
        return prefix, wildcard, suffix
    
    def _replaceWildcard(prefix, wildcard, suffix):
        if not os.path.exists(prefix):
            return []

        candidates = os.listdir(prefix)                        
        pattern = re.compile(re.sub(r"\*", ".*", wildcard))
        valid_candidate = [c for c in candidates if re.match(pattern, c)]
        
        result = []
        for c in valid_candidate:
            result.append(os.path.join(prefix, c, suffix))
        return result
    
    result = []
    for path in path_list:
        if "*" not in path:
            result.append(path)
        else:
            prefix, wildcard, suffix = _splitByWildcard(path)
            _res = _replaceWildcard(prefix, wildcard, suffix)
            result += _glob(_res)
    return result