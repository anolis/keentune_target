# Copyright (c) 2021-2023 Alibaba Cloud Computing Ltd.
# SPDX-License-Identifier: MulanPSL-2.0
# pylint: disable=C,redefined-outer-name,unspecified-encoding,import-error,logging-format-interpolation,broad-except,no-self-use,protected-access,abstract-method,consider-using-with,too-many-instance-attributes,too-many-arguments,eval-used,fixme,attribute-defined-outside-init
import os  
import json 
import logging

from abc import ABCMeta, abstractmethod
from agent.common.config import Config
from agent.common.macro import parseExpression

logger = logging.getLogger('common')

class BaseDomain(metaclass = ABCMeta):
    __domain__ = "base"
    
    @classmethod
    def isActived(self):
        """ Loading Check,  If backup file exists, this domain is actived.
        """
        backup_file = self.backup_file_path()
        if os.path.exists(backup_file):
            return True
    

    @classmethod
    def backup_file_path(self):
        """ Return backup file path
        
        Function 1. Backup: Save backup value in this path
        Function 2. Loading Check: If backup file exists, load domain to rollback when keentune-target started.
        """
        return os.path.join(Config.BACKUP_PATH, "{}.json".format(self.__domain__))
    

    @abstractmethod
    def _set_value(self, param_name, param_value):
        """ Implementation of single parameter setting method in domain

        Args:
            param_name (str): parameter name
            param_value (_type_): value to be set
        """
    

    @abstractmethod
    def _get_value(self, param_name):
        """ Implementation of single parameter reading method in domain

        Args:
            param_name (str): parameter name
        """
    

    def pre_setting(self):
        """ Processing before all parameters setting
        """


    def post_setting(self):
        """ Processing after all parameters setting
        """

    def post_rollback(self):
        """ Processing before rollback parameters
        """

    def set_values(self, param_list):
        """ Processing of setting all parameters

        """
        result = {}
        for param_name, param_info in param_list.items():
            try:
                # replace macro definition
                value = self._set_value(
                    param_name, 
                    parseExpression(param_info['value']))
                
                result[param_name] = {
                    "suc"  : True, 
                    "value": value,
                    "msg"  : "success"
                }
                
            except Exception as e:
                result[param_name] = {
                    "suc"  : False, 
                    "value": "",
                    "msg"  : f"{e}"
                }
                continue

        logger.info("'{}' param setting finished: {}".format(self.__domain__, result))
        return result


    def get_values(self, param_list):
        """ Reading parameter value

        """
        result = {}
        for param_name, _ in param_list.items():
            try:
                result[param_name] = {
                    "suc"   : True, 
                    "skip"  : False,
                    "value" : self._get_value(param_name), 
                    "msg"   : "success"
                }
                
            except Exception as e:
                result[param_name] = {
                    "suc"   : False, 
                    "skip"  : False,
                    "value" : "", 
                    "msg"   : f"{e}"
                }
        return result


    def backup(self, param_list):
        """ Backup parameters
        
        Return:
            dict: backup data
            bool: skip backuped param check, if false, skip param setting which disappeared in backup data
        """
        backup_data = {}
        for param_name in param_list.keys():
            try:
                backup_data[param_name] = self._get_value(param_name)

            except Exception as e:
                logger.error(f"domain {self.__domain__} read param {param_name} failed: {e}")
                continue
        
        with open(self.backup_file_path(), "w") as f:
            f.write(json.dumps(backup_data))
        logger.info("Save backup value {} to {}".format(backup_data, self.backup_file_path()))
        return backup_data, False


    def rollback(self):
        """ Rollback parameter from backup file

        """
        if not os.path.exists(self.backup_file_path()):
            return
        
        backup_data = json.load(open(self.backup_file_path()))
        
        ALL_SUCCESS = True
        for param_name, backup_value in backup_data.items():
            try:
                self._set_value(param_name, backup_value)

            except Exception as e:
                logger.error(f"domain {self.__domain__} rollback param {param_name} failed: {e}")
                ALL_SUCCESS = False
        
        # Try to rollback more parameters
        if ALL_SUCCESS:
            logger.info("'{}' rollback success, remove bakcup file".format(self.__domain__))
            os.remove(self.backup_file_path())
        else:
            logger.error(f"not all parameter rollback successful of domain {self.__domain__}")