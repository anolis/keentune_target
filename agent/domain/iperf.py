# Copyright (c) 2021-2023 Alibaba Cloud Computing Ltd.
# SPDX-License-Identifier: MulanPSL-2.0
# pylint: disable=C,redefined-outer-name,unspecified-encoding,import-error,logging-format-interpolation,broad-except,no-self-use,protected-access,abstract-method,consider-using-with,too-many-instance-attributes,too-many-arguments,eval-used,fixme,attribute-defined-outside-init

import os
import re
from agent.common.config import Config
from agent.domain.benchmark import BaseBenchmark

class Iperf(BaseBenchmark):
    __domain__ = "iperf"
    script_path = os.path.join(Config.BENCKMARK_FIlE, "benchmark/iperf/iperf.py")
    default_param =  {
        "Parallel": 1,
        "window_size": 10240,
        "length_buffers": 10240
    }
    parameters_list = [
        "Parallel",
        "window_size",
        "length_buffers",
    ]        

    def post_setting(self):
        args = " ".join(["-{} {}".format(k[0], v) for k,v in self.param_value.items()])
        with open(self.script_path, "r") as f:
            data = f.read()
            data = re.sub(r'DEFAULT = "[a-zA-Z0-9\-=\s]*"', 'DEFAULT = "{}"'.format(args), data)
            data = re.sub(r"PARALLEL = \d+", "PARALLEL = {}".format(self.param_value["Parallel"]), data)

        with open(self.script_path, "w") as f:
            f.write(data)
        self.param_value = self.default_param