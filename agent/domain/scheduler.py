# Copyright (c) 2021-2023 Alibaba Cloud Computing Ltd.
# SPDX-License-Identifier: MulanPSL-2.0
# pylint: disable=C,redefined-outer-name,unspecified-encoding,import-error,logging-format-interpolation,broad-except,no-self-use,protected-access,abstract-method,consider-using-with,too-many-instance-attributes,too-many-arguments,eval-used,fixme,attribute-defined-outside-init

import logging

from agent.domain.base import BaseDomain
from agent.common.system import sysCommand

logger = logging.getLogger('common')

class Scheduler(BaseDomain):
    __domain__ = "scheduler"
    __params__ = [
        'sched_min_granularity_ns',    # /proc/sys/kernel/sched_min_granularity_ns
        'sched_latency_ns',            # /proc/sys/kernel/sched_latency_ns
        'sched_features',              # /proc/sys/kernel/sched_features(3183d=110001101111b)
        'sched_wakeup_granularity_ns', # /proc/sys/kernel/sched_wakeup_granularity_ns(4000000ns)
        'sched_child_runs_first',      # /proc/sys/kernel/sched_child_runs_first(0)
        'sched_cfs_bandwidth_slice_us',# /proc/sys/kernel/sched_cfs_bandwidth_slice_us(5000us)
        'sched_rt_period_us',          # /proc/sys/kernel/sched_rt_period_us(1000000us)
        'sched_rt_runtime_us',         # /proc/sys/kernel/sched_rt_runtime_us(950000us)
        'sched_compat_yield',          # /proc/sys/kernel/sched_compat_yield(0)
        'sched_migration_cost',        # /proc/sys/kernel/sched_migration_cost(500000ns)
        'sched_nr_migrate',            # /proc/sys/kernel/sched_nr_migrate(32)
        'sched_tunable_scaling',       # /proc/sys/kernel/sched_tunable_scaling(1)
        'sched_migration_cost_ns',     # /proc/sys/kernel/sched_migration_cost_ns
    ]

    def _set_value(self, param_name:str, param_value):
        assert param_name in self.__params__
        assert str(param_value).isdigit()

        sysCommand(f"echo {param_value} > /proc/sys/kernel/{param_name}", log=True)
        return param_value


    def _get_value(self, param_name:str):
        return sysCommand(f"cat /proc/sys/kernel/{param_name}")