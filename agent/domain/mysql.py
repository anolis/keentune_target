# Copyright (c) 2021-2023 Alibaba Cloud Computing Ltd.
# SPDX-License-Identifier: MulanPSL-2.0
# pylint: disable=C,redefined-outer-name,unspecified-encoding,import-error,logging-format-interpolation,broad-except,no-self-use,protected-access,abstract-method,consider-using-with,too-many-instance-attributes,too-many-arguments,eval-used,fixme,attribute-defined-outside-init

import os
import re
import logging

from agent.common.system import sysCommand
from agent.domain.fileconfig import FileConfig
from agent.common.config import ConfigFile


logger = logging.getLogger('common')


class MySQL(FileConfig):
    __domain__ = "my_cnf"
    __config_file__ = ConfigFile.conf['mysql']['MYSQL_CONF']

    __config_head__ = "[client-server] \n" \
                    "!includedir /etc/my.cnf.d \n" \
                    "[mysqld] \n"
    __bool_param__  = ["core-file", "skip_ssl", "skip_name_resolve"]
    __get_bool_param__ = "cat /etc/my.cnf | grep -w {param_name} | tail -n1 | wc -L | \
                awk '{{if (sum=$(wc -L \"{param_name}\") && $0==$sum) print \"SET_TRUE\";else print \"SET_FALSE\"}}'"
    __get_param__ = "cat /etc/my.cnf | grep -w {param_name} | tail -n1 | awk -F'=' '{{print$2}}'"


    def __init__(self):
        super().__init__()
        # assert mysql service is exists
        sysCommand("systemctl start mysqld")


    def pre_setting(self):
        if not os.path.exists(self.backup_file_path()):
            logger.warning("backup mySQL file not exists, stop setting")
            raise Exception("backup mySQL file not exists, stop setting")

        with open(self.__config_file__, "w") as f:
            f.write(self.__config_head__)
        

    def post_setting(self):
        """ Restart mysql service after param setting

        """
        try:
            sysCommand("systemctl restart mysqld", log = True)
        except Exception as e:
            logger.error(f"restart mysqld failed: {e}")
            raise


    def post_rollback(self):
        try:
            sysCommand("systemctl restart mysqld", log = True)
        except Exception as e:
            logger.error(f"restart mysqld failed: {e}")
            raise


    def _get_value(self, param_name):
        assert re.match(r"^[\w\-]+$", str(param_name))

        if param_name in self.__bool_param__:
            return sysCommand(self.__get_bool_param__.format(param_name = param_name))
        return sysCommand(self.__get_param__.format(param_name = param_name))


    def _set_value(self, param_name, param_value):
        if param_name in ["core-file", "skip_ssl", "skip_name_resolve"] and param_value == "SET_TRUE":
            sysCommand(f"echo {param_name} >> /etc/my.cnf", log = True)
        
        else:
            assert re.match(r"^[\w\-]+$", str(param_name))
            assert re.match(r"^[\w\-]+$", str(param_value))
            sysCommand(f"echo {param_name}={param_value} >> /etc/my.cnf", log = True)

        return param_value