# Copyright (c) 2021-2023 Alibaba Cloud Computing Ltd.
# SPDX-License-Identifier: MulanPSL-2.0
# pylint: disable=C,redefined-outer-name,unspecified-encoding,import-error,logging-format-interpolation,broad-except,no-self-use,protected-access,abstract-method,consider-using-with,too-many-instance-attributes,too-many-arguments,eval-used,fixme,attribute-defined-outside-init

import os
import logging

from agent.domain.base import BaseDomain
from agent.common.config import Config
from agent.common.system import sysCommand
from agent.common.exception import DomainBasicException

logger = logging.getLogger('common')

class Env(BaseDomain):
    __domain__ = "env"

    def _set_value(self, param_name:str, param_value):
        assert param_value in ['start', 'stop', 'verify']
        assert param_name in ['erdma', 'set_irq', 'set_xps_rps']

        script_path = os.path.join(Config.SCRIPTS_PATH, param_name + ".sh")
        if not os.path.exists(script_path):
            raise DomainBasicException("can not find script {}".format(script_path), self.__domain__)

        sysCommand("{script_path} {operation}".format(
                script_path = script_path, 
                operation = param_value), log = True)
        return param_value


    def _get_value(self, param_name:str):
        return "stop"
