# Copyright (c) 2021-2023 Alibaba Cloud Computing Ltd.
# SPDX-License-Identifier: MulanPSL-2.0
# pylint: disable=C,redefined-outer-name,unspecified-encoding,import-error,logging-format-interpolation,broad-except,no-self-use,protected-access,abstract-method,consider-using-with,too-many-instance-attributes,too-many-arguments,eval-used,fixme,attribute-defined-outside-init

import os
from agent.common.config import Config
from agent.domain.benchmark import BaseBenchmark

class Fio(BaseBenchmark):
    __domain__ = "fio"
    script_path = os.path.join(Config.BENCKMARK_FIlE, "benchmark/fio/fio_IOPS_base.py")
    default_param =  {
        "iodepth": 1,
        "bs": "512B",
        "numjobs": 8
    }
    parameters_list = [
        "iodepth",
        "bs",
        "numjobs",
    ]