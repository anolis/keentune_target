import os
import logging

from agent.domain.base import BaseDomain
from agent.common.system import sysCommand
from agent.common.config import Config

logger = logging.getLogger('common')

""" 
Abstract class of domains 
which set param by config file writng and backup by copy this file.
"""

class FileConfig(BaseDomain):
    __domain__ = "fileconfig"
    __config_file__ = "/etc/profile"

    @classmethod
    def backup_file_path(self):
        return os.path.join(
            Config.BACKUP_PATH, 
            self.__domain__ + "_" + os.path.split(self.__config_file__)[-1])


    def backup(self, _):
        """
                    Backup Check Table
            | Backupfile | Configfile | Operation |
            |   exists   |     *      |  no need  |
            | not exists |   exists   |   backup  |
            | not exists | not exists |   error   |
        """

        if os.path.exists(self.backup_file_path()):
            logger.info("backup file '{}' exists, skip backup".format(self.backup_file_path()))
            return None, True
        
        try:
            sysCommand("cp -f {} {}".format(self.__config_file__, self.backup_file_path()), log = True)
            logger.info("copy {} to {} to backup".format(self.__config_file__, self.backup_file_path()))

        except Exception as e:
            logger.error(f"can not bakcup file '{self.__config_file__}': {e}")
            raise

        return None, True


    def rollback(self):
        """
                    Rollback Check Table
            | Backupfile | Configfile | Operation         |
            | not exists |     *      | no need           |
            |   exists   |   exists   | checking rollback | TODO
            |   exists   | not exists | clear rollback    |
        """
        if not os.path.exists(self.backup_file_path()):
            return

        try:
            sysCommand("mv -f {} {}".format(self.backup_file_path(), self.__config_file__), log = True)
        except Exception as e:
            logger.info(f"can not rollback file '{self.__config_file__}': {e}")
            raise