# Copyright (c) 2021-2023 Alibaba Cloud Computing Ltd.
# SPDX-License-Identifier: MulanPSL-2.0
# pylint: disable=C,redefined-outer-name,unspecified-encoding,import-error,logging-format-interpolation,broad-except,no-self-use,protected-access,abstract-method,consider-using-with,too-many-instance-attributes,too-many-arguments,eval-used,fixme,attribute-defined-outside-init

import re
import logging

from agent.common.system import sysCommand
from agent.domain.base import BaseDomain

logger = logging.getLogger('common')

class Limits(BaseDomain):
    __domain__ = "limits"
    __pattern__ = r"(.+) {} (\d+)\n*"
    __limits_config__   = "/etc/security/limits.conf"

    def _set_backup_value(self, param_name, param_value):
        print(f"set backup value: {param_value}")
        limits_config = sysCommand("cat {}".format(self.__limits_config__))
        limits_config = re.sub(self.__pattern__.format(param_name), "", limits_config).strip()
        
        for scope, value in param_value.items():
            logger.info(f"{scope} {param_name} {value}")
            limits_config += f"\n{scope} {param_name} {value}"
        
        with open(self.__limits_config__, "w") as f:
            f.write(limits_config)


    def _set_value(self, param_name:str, param_value):
        print(f"set value :{param_value}")
        
        if type(param_value) is dict:
            self._set_backup_value(param_name, param_value)
            return
        
        param_name = re.sub(r"_", " ", param_name)
        limits_config = sysCommand("cat {}".format(self.__limits_config__))
        limits_config = re.sub(self.__pattern__.format(param_name), "", limits_config).strip()

        logger.info(f"append config: 'root {param_name} {param_value}'")
        limits_config += f"\nroot {param_name} {param_value}"
        
        logger.info(f"append config: '* {param_name} {param_value}'")
        limits_config += f"\n* {param_name} {param_value}"
        
        with open(self.__limits_config__, "w") as f:
            f.write(limits_config)
        return param_value

    
    def _get_value(self, param_name:str):
        param_name = re.sub(r"_", " ", param_name)
        limits_config = sysCommand("cat {}".format(self.__limits_config__))

        values = {}
        for match in re.findall(self.__pattern__.format(param_name), limits_config):
            values[match[0]] = match[1]
        
        return values
