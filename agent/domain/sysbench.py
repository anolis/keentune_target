# Copyright (c) 2021-2023 Alibaba Cloud Computing Ltd.
# SPDX-License-Identifier: MulanPSL-2.0
# pylint: disable=C,redefined-outer-name,unspecified-encoding,import-error,logging-format-interpolation,broad-except,no-self-use,protected-access,abstract-method,consider-using-with,too-many-instance-attributes,too-many-arguments,eval-used,fixme,attribute-defined-outside-init

import os
import re
from agent.common.config import Config
from agent.domain.benchmark import BaseBenchmark

class Sysbench(BaseBenchmark):
    __domain__ = "sysbench"
    script_path = os.path.join(Config.BENCKMARK_FIlE, "benchmark/sysbench/sysbench_mysql_read_write.py")
    default_param = {
        "thread-stack-size": 32768,
        "table-size": 100000,
        "tables": 3,
        "threads": 1
    }
    parameters_list = [
        "thread-stack-size",
        "table-size",
        "tables",
        "threads"
    ]        

    def post_setting(self):
        args = " ".join(["--{}={}".format(k, v) for k,v in self.param_value.items()])
        with open(self.script_path, "r") as f:
            data = f.read()
            data = re.sub(r'DEFAULT = "[a-zA-Z0-9\-=\s]*"', 'DEFAULT = "{}"'.format(args), data)

        with open(self.script_path, "w") as f:
            f.write(data)
        self.param_value = self.default_param