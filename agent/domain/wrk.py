# Copyright (c) 2021-2023 Alibaba Cloud Computing Ltd.
# SPDX-License-Identifier: MulanPSL-2.0
# pylint: disable=C,redefined-outer-name,unspecified-encoding,import-error,logging-format-interpolation,broad-except,no-self-use,protected-access,abstract-method,consider-using-with,too-many-instance-attributes,too-many-arguments,eval-used,fixme,attribute-defined-outside-init

import os
import re

from agent.common.config import Config
from agent.domain.benchmark import BaseBenchmark

class Wrk(BaseBenchmark):
    __domain__ = "wrk"
    script_path = os.path.join(Config.BENCKMARK_FIlE, "benchmark/wrk/wrk_parameter_tuning.py")
    default_param = {
        "connections": 10,
        "threads": 2
    }
    parameters_list = [
        "connections",
        "duration",
        "threads",
        "script",
        "header",
        "latency",
        "timeout",
        "version"
    ]

    def post_setting(self):
        args = " ".join(["--{} {}".format(k, v) for k,v in self.param_value.items()])
        with open(self.script_path, "r") as f:
            data = f.read()
            data = re.sub(r'DEFAULT = "[a-zA-Z0-9\-=\s]*"', 'DEFAULT = "{}"'.format(args), data)

        with open(self.script_path, "w") as f:
            f.write(data)
        self.param_value = self.default_param