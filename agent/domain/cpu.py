# Copyright (c) 2021-2023 Alibaba Cloud Computing Ltd.
# SPDX-License-Identifier: MulanPSL-2.0
# pylint: disable=C,redefined-outer-name,unspecified-encoding,import-error,logging-format-interpolation,broad-except,no-self-use,protected-access,abstract-method,consider-using-with,too-many-instance-attributes,too-many-arguments,eval-used,fixme,attribute-defined-outside-init

import os
import logging

from agent.domain.base import BaseDomain
from agent.common.system import sysCommand

logger = logging.getLogger('common')

class Cpu(BaseDomain):
    __domain__ = "cpu"
    __intel_pstate_path__ = "/sys/devices/system/cpu/intel_pstate"

    def _set_value(self, param_name:str, param_value):
        assert param_name in ['min_perf_pct', 'max_perf_pct']
        assert int(param_value) >=0 and int(param_value) <= 100

        sysCommand("echo {} > {}".format(
            param_value,
            os.path.join(self.__intel_pstate_path__, param_name)
        ), log = True)
        return param_value


    def _get_value(self, param_name:str):
        return sysCommand("cat {}".format(
            os.path.join(self.__intel_pstate_path__, param_name)
        ), log = True)
