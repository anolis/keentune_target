# Copyright (c) 2021-2023 Alibaba Cloud Computing Ltd.
# SPDX-License-Identifier: MulanPSL-2.0
# pylint: disable=C,redefined-outer-name,unspecified-encoding,import-error,logging-format-interpolation,broad-except,no-self-use,protected-access,abstract-method,consider-using-with,too-many-instance-attributes,too-many-arguments,eval-used,fixme,attribute-defined-outside-init

import logging
import pynginxconfig

from agent.common.system import sysCommand
from agent.domain.fileconfig import FileConfig
from agent.common.config import ConfigFile

logger = logging.getLogger('common')

class Nginx(FileConfig):
    __domain__      = "nginx_conf"
    __config_file__ = ConfigFile.conf['nginx']['NGINX_CONF']

    __log_value__   = """main  '$remote_addr - $remote_user [$time_local] "$request" '
                '$status $body_bytes_sent "$http_referer" '
                '"$http_user_agent" "$http_x_forwarded_for"'"""

    __param_base__  = [
        "worker_processes", 
        "worker_rlimit_nofile", 
        "worker_cpu_affinity",
        "ssl_engine"
    ]

    __param_events__= [
        "worker_connections", 
        "multi_accept"
    ]

    __param_qat__   = []

    def __init__(self):
        super().__init__()
        self.nginx_conf = pynginxconfig.NginxConfig()
        self.nginx_conf.loadf(self.__config_file__)

        # assert nginx service is exists
        sysCommand("systemctl start nginx")
        self.qatengine = False


    def pre_setting(self):
        try:
            logger.info("set default 'log_format' and 'access_log'")
            if self.nginx_conf.get([("http",), "log_format"]):
                self.nginx_conf.set([("http",), "log_format"], self.__log_value__)
                
            if self.nginx_conf.get([("http",), "access_log"]):
                self.nginx_conf.set([("http",), "access_log"], "off")
        except Exception as e:
            raise ("set 'log_format' and 'access_log' failed:{}".format(e), self.__domain__)


    def post_setting(self):
        """ Restart nginx service after param setting

        """
        try:
            logger.info("save nginx config: {}".format(self.nginx_conf))
            self.nginx_conf.savef(self.__config_file__)

        except Exception as e:
            logger.error(f"save nginx config failed: {e}")
            raise

        if self.qatengine:
            sysCommand("yum install qatengine -y", log=True)
        
        try:
            sysCommand("systemctl restart nginx", log = True)
        except Exception as e:
            logger.error(f"restart nginx failed: {e}")
            raise
    

    def post_rollback(self):
        sysCommand("yum remove qatengine -y", log=True)
        
        try:
            sysCommand("systemctl restart nginx", log = True)
        except Exception as e:
            logger.error(f"restart nginx failed: {e}")
            raise


    def _get_value(self, param_name):
        """ Read value of a parameter in nginx.conf

        """
        try:
            if param_name in self.__param_base__:
                return self.nginx_conf.get(param_name)[1]
            
            if param_name in self.__param_events__:
                return self.nginx_conf.get([("events",), param_name])[1]
            
            elif param_name in self.__param_qat__:
                return self.nginx_conf.get([("ssl_engine",), ("qat_engine",), param_name])[1]
            
            return self.nginx_conf.get([("http",), param_name])[1]
        
        except Exception as e:
            logger.warning(f"read '{param_name}' value error:{e}")
            return ""
    

    def _set_value(self, param_name, param_value):
        """ Set value of a parameter in nginx.conf
        """
        try:
            param_value = "{}".format(param_value).strip()
            if param_name == "ssl_engine":
                self.qatengine = (param_value=="qatengine")

            if param_name in self.__param_base__:
                if self.nginx_conf.get(param_name):
                    self.nginx_conf.set(param_name, param_value)
                    logger.info("set param {} = {}".format(param_name, param_value))
                else:
                    self.nginx_conf.append((param_name, param_value), position=4)
                    logger.info("append param {} = {}".format(param_name, param_value))
            
            elif param_name in self.__param_events__:
                if self.nginx_conf.get([("events",), param_name]):
                    self.nginx_conf.set([("events",), param_name], param_value)
                    logger.info("set events param {} = {}".format(param_name, param_value))
                else:
                    self.__append_param("events", (param_name, param_value), 1)
                    logger.info("append events param {} = {}".format(param_name, param_value))

            elif param_name in self.__param_qat__:
                if self.nginx_conf.get([("ssl_engine",), ("qat_engine",), param_name]):
                    self.nginx_conf.set([("ssl_engine",), ("qat_engine",), param_name], param_value)
                    logger.info("set events param {} = {}".format(param_name, param_value))
            
            else:
                if self.nginx_conf.get([("http",), param_name]):
                    self.nginx_conf.set([("http",), param_name], param_value)
                    logger.info("set http param {} = {}".format(param_name, param_value))
                else:
                    self.__append_param("http", (param_name, param_value), 7)
                    logger.info("append http param {} = {}".format(param_name, param_value))
        
        except Exception as e:
            logger.error(f"nginx: set value {param_name}={param_value} failed: {e}")
            raise

        return param_value


    def __append_param(self, param, item, position = 1):
        """ Append a parameter and value to nginx.conf

        """
        for index, element in enumerate(self.nginx_conf.data):
            if isinstance(element, dict) and element["name"] == param:
                self.nginx_conf.data[index]["value"].insert(position, item)
