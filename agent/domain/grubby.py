# Copyright (c) 2021-2023 Alibaba Cloud Computing Ltd.
# SPDX-License-Identifier: MulanPSL-2.0
# pylint: disable=C,redefined-outer-name,unspecified-encoding,import-error,logging-format-interpolation,broad-except,no-self-use,protected-access,abstract-method,consider-using-with,too-many-instance-attributes,too-many-arguments,eval-used,fixme,attribute-defined-outside-init

import re  
import logging

from agent.common.system import sysCommand
from agent.domain.base import BaseDomain
from agent.common.exception import InitException

logger = logging.getLogger('common')

class Grub(BaseDomain):
    """ Configure grub parameter by grubby

    """
    __domain__ = "grubby"
    __load_kernel__ = "grubby --default-kernel"
    __kernel_info__ = "grubby --info={kernel_name}"
    __append_args__ = "grubby --update-kernel={kernel_name} --args={param_name}"
    __remove_args__ = "grubby --update-kernel={kernel_name} --remove-args={param_name}"
    
    def __init__(self):
        super().__init__()
        try:
            self.default_kernel = sysCommand(self.__load_kernel__)
            logger.info("default kernel: {}".format(self.default_kernel))
        except Exception as e:
            raise InitException("load default kernel failed:{}".format(e), self.__domain__)


    def _set_value(self, param_name:str, param_value):
        assert param_name in ['spectre_bhb']

        current_value = self._get_value(param_name)
        if param_value == current_value:
            logger.info("skip same value: {}".format(current_value))
            return
        
        if param_value in ["on", "off", "none"]:
            # clear exists param setting
            sysCommand(self.__remove_args__.format(
                kernel_name = self.default_kernel,
                param_name = param_name,
            ), log = True)
            sysCommand(self.__remove_args__.format(
                kernel_name = self.default_kernel,
                param_name = "no" + param_name,
            ), log = True)
        
            if param_value == "on":
                sysCommand(self.__append_args__.format(
                    kernel_name = self.default_kernel,
                    param_name = param_name
                ), log = True)
            
            if param_value == "off":
                sysCommand(self.__append_args__.format(
                    kernel_name = self.default_kernel,
                    param_name = "no" + param_name
                ), log = True)

        else:
            sysCommand(self.__append_args__.format(
                kernel_name = self.default_kernel,
                param_name = "{}={}".format(param_name, param_value)
            ), log = True)
        return param_value


    def _get_value(self, param_name:str):
        """ Read param current value by 'grubby --info=[default_kernel]'

        """
        kernel_info = sysCommand(self.__kernel_info__.format(kernel_name = self.default_kernel))
        args_info = re.search("args=\"(.*)\"", kernel_info).group(1)

        if re.search(r"{}=(.*)".format(param_name), args_info):
            return re.search(r"{}=(.*)".format(param_name), args_info).group(1)

        if re.search("no{}".format(param_name), args_info):
            return "off"

        if re.search("{}".format(param_name), args_info):
            return "on"

        return "none"