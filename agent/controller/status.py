# Copyright (c) 2021-2023 Alibaba Cloud Computing Ltd.
# SPDX-License-Identifier: MulanPSL-2.0
# pylint: disable=C,redefined-outer-name,unspecified-encoding,import-error,logging-format-interpolation,broad-except,no-self-use,protected-access,abstract-method,consider-using-with,too-many-instance-attributes,too-many-arguments,eval-used,fixme,attribute-defined-outside-init

import json
import logging
import re

from tornado.web import RequestHandler

from agent.controller import Context
from agent.common.system import sysCommand
from agent.common.macro import _virt

logger = logging.getLogger('common')

class StatusHandler(RequestHandler):
    def get(self):
        self.write(json.dumps({"status": "alive"}))
        self.finish()


class AvailableDomainHandler(RequestHandler):
    context = Context()

    def get(self):
        logger.info("================ Get GET:/available requests ================")
        
        try:
            self.available_data = {
                "feature": {
                    "actived": list(self.context.feature_object.keys()),
                    "available": list(self.context.feature_class.keys())
                },
                "domain": {
                    "actived": list(self.context.domain_object.keys()),
                    "available": list(self.context.domain_class.keys())
                }
            }
        
        except Exception:
            self.set_status(500)
            self.finish()

        else:
            logger.info("[+] return data: {}".format(self.available_data))
            self.write(json.dumps(self.available_data))
            self.finish()


class isVirtualHandler(RequestHandler):
    def get(self):
        logger.info("================ Get GET:/virt requests ================")

        self.virt_data = {"virt": _virt()}
        logger.info("[+] return data: {}".format(self.virt_data))
        self.write(json.dumps(self.virt_data))
        self.finish()


class OSReleaseHandler(RequestHandler):
    def get(self):
        logger.info("================ Get GET:/os_release requests ================")

        try:
            os_release = sysCommand("cat /etc/os-release")
            self.distribution_data = {
                "name": re.search(r"NAME=\"(.*)\"",os_release).group(1),
                "version":re.search(r"VERSION=\"(.*)\"",os_release).group(1),
                "platform": re.search(r"PLATFORM_ID=\"(.*)\"",os_release).group(1),
            }
        except Exception as e:
            self.distribution_data = {
                "err": e
            }
        
        logger.info("[+] return data: {}".format(self.distribution_data))
        self.write(json.dumps(self.distribution_data))
        self.finish()
