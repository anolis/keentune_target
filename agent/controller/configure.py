# Copyright (c) 2021-2023 Alibaba Cloud Computing Ltd.
# SPDX-License-Identifier: MulanPSL-2.0
# pylint: disable=C,redefined-outer-name,unspecified-encoding,import-error,logging-format-interpolation,broad-except,no-self-use,protected-access,abstract-method,consider-using-with,too-many-instance-attributes,too-many-arguments,eval-used,fixme,attribute-defined-outside-init

import json
import logging
from collections import defaultdict
from tornado.web import RequestHandler
from collections import defaultdict

from agent.controller import Context
from agent.common.macro import parseCondition
from agent.common.exception import *

logger = logging.getLogger('common')

class ConfigureHandler(RequestHandler):
    context = Context()

    def post(self):

        self.readonly         = json.loads(self.request.body)["readonly"]
        self.request_data     = json.loads(self.request.body)['data']
        self.configure_result = defaultdict(dict)
        self.configure_data   = defaultdict(dict)

        logger.info("================ Get POST:/configure requests ================")
        logger.info("[+] request data: {}".format(self.request_data))

        # merge domain with different suffix
        self._filterLimitation()

        for domain_name, param_list in self.configure_data.items():
            self.configure_result[domain_name] = defaultdict(dict)
            
            for param_name, param_info in param_list.items():
                self._try_feature_handler(domain_name, param_name, param_info)
            
            # Remove handled parameter in feature handler
            for param_name in self.configure_result[domain_name]['detail'].keys():
                param_list.pop(param_name)        

            self._try_domain_handler(domain_name, param_list)

        logger.info("[+] return data: {}".format(self.configure_result))
        self.write(json.dumps(self.configure_result))
        self.finish()


    def _filterLimitation(self):
        """ Filter env-limitation and merge domain with different suffix

        """
        for domain_name, param_list in self.request_data.items():
            if param_list.__contains__('__env_limitation'):
                __env_limitation = param_list.pop("__env_limitation")
                if param_list.__contains__('__env_description'):
                    __env_description = param_list.pop("__env_description")['value']
                else:
                    __env_description = __env_limitation['value']
                
                try:
                    env_support = parseCondition(__env_limitation['value'])
                    if not env_support:
                        self.configure_result[domain_name]['suc']  = True
                        self.configure_result[domain_name]['skip'] = True
                        self.configure_result[domain_name]['msg']  = f"Environment restricted: '{__env_description}'"
                        continue

                except Exception as e:
                    self.configure_result[domain_name]['suc']  = False
                    self.configure_result[domain_name]['skip'] = False
                    self.configure_result[domain_name]['msg']  = f"parse '{__env_limitation['value']}' failed: {e}"
                    continue

            self.configure_data[domain_name.split(".")[0].lower()].update(param_list)
        
        logger.info("merge different suffix data: {}".format(self.configure_data))        
    

    def _try_feature_handler(self, domain_name, param_name, param_info):
        try:
            feature_handler = self.context.searchFeatureHandler(domain_name, param_name)

        except Exception as e:
            # Handler unavailable, there is no need to try again in domain level
            logger.warning(f"fail to init feature {domain_name}:{param_name}: {e}")
            self.configure_result[domain_name]['detail'][param_name] = {
                "suc"   : False, 
                "skip"  : True,
                "value" : "", 
                "msg"   : f"{e}"
            }
            return
        
        # No such feature handler
        if feature_handler is None:
            return
        
        logger.info("-------- Find Feature level handler '{}' --------".format(param_name))
        
        #* Reading parameter by feature
        if self.readonly:
            try:
                param_value = feature_handler.get_value()
                self.configure_result[domain_name]['detail'][param_name] = {
                    "suc"   : True, 
                    "skip"  : False,
                    "value" : param_value, 
                    "msg"   : ""
                }
            
            except Exception as e:
                logger.error(f"can not read param value: {e}")
                self.configure_result[domain_name]['detail'][param_name] = {
                    "suc"   : False, 
                    "skip"  : False,
                    "value" : "", 
                    "msg"   : f"failed to read parameter value"
                }
            return
        
        #* Setting parameter by feature
        try:
            logger.info(f"{domain_name}:{param_name}: rollback before setting.")
            feature_handler.rollback() 
        
        except Exception as e:
            logger.error(f"{domain_name}:{param_name}: rollback error terminate parameter setting: {e}")
            self.configure_result[domain_name]['detail'][param_name] = {
                "suc"   : False, 
                "skip"  : True,
                "value" : param_info['value'], 
                "msg"   : f"rollback failed: {e}"
            }
            return
        
        try:
            logger.info(f"{domain_name}:{param_name}: backup value to be set")
            feature_handler.backup()
        except Exception as e:
            logger.error(f"{domain_name}:{param_name}: backup error terminate parameter setting: {e}")
            self.configure_result[domain_name]['detail'][param_name] = {
                "suc"   : False, 
                "skip"  : True,
                "value" : param_info['value'], 
                "msg"   : f"backup failed: {e}"
            }
            return
        
        try:
            logger.info(f"{domain_name}:{param_name}: setting value: {param_info['value']}")
            feature_handler.set_value(value = param_info['value'])
        except Exception as e:
            logger.error(f"{domain_name}:{param_name} setting error: {e}")
            self.configure_result[domain_name]['detail'][param_name] = {
                "suc"   : False, 
                "skip"  : False,
                "value" : param_info['value'], 
                "msg"   : f"{e}"
            }
        else:
            self.configure_result[domain_name]['detail'][param_name] = {
                "suc"   : True, 
                "skip"  : False,
                "value" : param_info['value'], 
                "msg"   : "success"
            }
    

    def _skip_unbackup_param(self, domain_name, backuped_value, param_list):
        """ Remove unbackuped parameter from param_list

        """
        backuped_param_list = {}
        for param_name, param_info in param_list.items():
            if backuped_value.__contains__(param_name):
                backuped_param_list[param_name] = param_info
            else:
                logger.warning("skip unbackuped param '{}'".format(param_name))
                self.configure_result[domain_name]['detail'][param_name] = {
                    "suc"   : False,
                    "skip"  : True,
                    "value" : "",
                    "msg"   : "backup failed"
                }
        return backuped_param_list
    

    def _try_domain_handler(self, domain_name, param_list):
        if param_list.__len__() == 0:
            self.configure_result[domain_name]['suc']  = True
            self.configure_result[domain_name]['skip'] = False
            self.configure_result[domain_name]['msg']  = "setting parameters successful"
            return
        
        try:
            domain_handler = self.context.searchDomainHandler(domain_name)

        except Exception as e:
            logger.error(f"{domain_name}: Parameter setter unavaliable: {e}")
            self.configure_result[domain_name]['suc']  = False
            self.configure_result[domain_name]['skip'] = False
            self.configure_result[domain_name]['msg']  = f"parameter setter unavaliable: {e}"
            return
        
        logger.info("-------- Find domain level handler '{}' --------".format(domain_name))

        #* Reading parameter by domain
        if self.readonly:
            try:
                reading_value = domain_handler.get_values(param_list)
                for param_name, res in reading_value.items():
                    self.configure_result[domain_name]['detail'][param_name] = res
                self.configure_result[domain_name]['suc']  = True
                self.configure_result[domain_name]['skip'] = False
                self.configure_result[domain_name]['msg']  = "reading parameters successful"
                return
            
            except Exception as e:
                logger.error(f"{domain_name}: Parameter reading failed: {e}")
                self.configure_result[domain_name]['suc']  = False
                self.configure_result[domain_name]['skip'] = False
                self.configure_result[domain_name]['msg']  = f"reading parameters failed: {e}"
                return

        #* Setting parameter by domain
        # 1. rollback all backuped parameter
        try:
            logger.info(f"{domain_name}: rollback before setting")
            domain_handler.rollback()
        except Exception as e:
            logger.error(f"{domain_name}: rollback error terminate parameter setting: {e}")
            self.configure_result[domain_name]['suc']  = False
            self.configure_result[domain_name]['skip'] = False
            self.configure_result[domain_name]['msg']  = f"rollback error terminate parameter setting: {e}"
            return
        
        # 2. backup parameters to be set
        try:
            logger.info(f"{domain_name}: backup value to be set")
            backuped_value, noskip = domain_handler.backup(param_list)
            if not noskip:
                param_list = self._skip_unbackup_param(domain_name, backuped_value, param_list)
        except Exception as e:
            logger.error(f"{domain_name}: backup error terminate parameter setting: {e}")
            self.configure_result[domain_name]['suc']  = False
            self.configure_result[domain_name]['skip'] = False
            self.configure_result[domain_name]['msg']  = f"backup error terminate parameter setting: {e}"
            return

        # 3. setting parameter value
        try:
            logger.info(f"{domain_name}: start setting parameters: {param_list}")
            domain_handler.pre_setting()
            setting_result = domain_handler.set_values(param_list)
            domain_handler.post_setting()

        except Exception as e:
            logger.error(f"{domain_name}: setting parameters failed: {e}")
            self.configure_result[domain_name]['suc']  = False
            self.configure_result[domain_name]['skip'] = False
            self.configure_result[domain_name]['msg']  = f"setting parameters failed: {e}"
            return
        
        else:
            for param_name, res in setting_result.items():
                self.configure_result[domain_name]['detail'][param_name] = res
            self.configure_result[domain_name]['suc']  = True
            self.configure_result[domain_name]['skip'] = False
            self.configure_result[domain_name]['msg']  = "setting parameters successful"
