# Copyright (c) 2021-2023 Alibaba Cloud Computing Ltd.
# SPDX-License-Identifier: MulanPSL-2.0
# pylint: disable=C,redefined-outer-name,unspecified-encoding,import-error,logging-format-interpolation,broad-except,no-self-use,protected-access,abstract-method,consider-using-with,too-many-instance-attributes,too-many-arguments,eval-used,fixme,attribute-defined-outside-init

import json
import logging

from tornado.web import RequestHandler
from agent.controller import Context

logger = logging.getLogger('common')

class RollbackHandler(RequestHandler):
    context = Context()

    def get(self):
        logger.info("================ Get GET:/rollback requests ================")

        rollback_result = self.context.rollback()

        if rollback_result.__len__() == 0:
            return_data = {"suc": True, "res": None}
        else:
            return_data = {"suc": False, "res": rollback_result}
        
        logger.info("[+] return data: {}".format(return_data))
        self.write(json.dumps(return_data))
        self.finish()
