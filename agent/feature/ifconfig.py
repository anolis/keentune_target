# Copyright (c) 2021-2023 Alibaba Cloud Computing Ltd.
# SPDX-License-Identifier: MulanPSL-2.0
# pylint: disable=C,redefined-outer-name,unspecified-encoding,import-error,logging-format-interpolation,broad-except,no-self-use,protected-access,abstract-method,consider-using-with,too-many-instance-attributes,too-many-arguments,eval-used,fixme,attribute-defined-outside-init
import re
import logging

from agent.common.system import sysCommand
from agent.feature.base import Feature
from agent.common.netinfo import NetInfo
from agent.common.exception import InitException

logger = logging.getLogger("common")

class Ifconfig(Feature):
    __domain__  = "ifconfig"
    __feature__ = "mtu"
    __set_cmd__ = "ifconfig {dev_name} mtu {value}"
    __get_cmd__ = "ifconfig {dev_name}"

    def __init__(self):
        super().__init__()
        try:
            sysCommand("ifconfig")  #! if 'ifconfig' unavailable, raise exception
        except Exception:
            raise InitException("'ifconfig' unavailable", self.__domain__, self.__feature__)
        self.net_info = NetInfo()
    

    def set_value(self, value):
        assert str(value).isdigit()
        sysCommand(self.__set_cmd__.format(
                dev_name = self.net_info.dev_name,
                value = value), log = True)
        return value
    

    def get_value(self):
        ifconfig_info = sysCommand(self.__get_cmd__.format(dev_name = self.net_info.dev_name))
        if re.search(r"mtu (\d+)", ifconfig_info):
            return re.search(r"mtu (\d+)", ifconfig_info).group(1)
        raise Exception("can not get mtu of {}".format(self.net_info.dev_name))
