# Copyright (c) 2021-2023 Alibaba Cloud Computing Ltd.
# SPDX-License-Identifier: MulanPSL-2.0
# pylint: disable=C,redefined-outer-name,unspecified-encoding,import-error,logging-format-interpolation,broad-except,no-self-use,protected-access,abstract-method,consider-using-with,too-many-instance-attributes,too-many-arguments,eval-used,fixme,attribute-defined-outside-init
import logging
import subprocess
import re

from agent.common.system import sysCommand
from agent.feature.base import Feature
from agent.common.netinfo import NetInfo


logger = logging.getLogger('common')


class SmpAffinity(Feature):
    __domain__  = "net"
    __feature__ = "smp_affinity"

    __get_irqs__ = "cat /proc/irq/{interrupt_id}/smp_affinity_list"
    __set_irqs__ = "echo {core} > /proc/irq/{interrupt_id}/smp_affinity_list"

    def __init__(self) -> None:
        super().__init__()
        self.net_info = NetInfo()
    

    def get_value(self):
        try:
            sysCommand("systemctl status irqbalance.service")
        except subprocess.CalledProcessError:
            logger.info("[irqs] irqbalance.service is inactive")
            irqbalance_service = False
        else:
            logger.info("[irqs] irqbalance.service is active")
            irqbalance_service = True

        irqs_value = {}
        for interrupt_id in self.net_info.interrupts:
            irqs_value[interrupt_id] = sysCommand(self.__get_irqs__.format(
                                        interrupt_id = interrupt_id))
        
        return {
            "service": irqbalance_service,
            "irqs": irqs_value
        }


    def _set_old_version_backup_value(self, backup_value):
        """
        Compatible with versions 2.4.0 and earlier which save backupfile as {"27": "0"}

        """
        logger.info("set backup value by old version")
        for interrupt_id, core in backup_value.items():
            sysCommand(self.__set_irqs__.format(
                core = core,
                interrupt_id = interrupt_id
            ), log = True)


    def _set_backup_value(self, backup_value):
        logger.info("[irqs] set backup irqs value: {}".format(backup_value))
        if not backup_value.__contains__('service'):
            self._set_old_version_backup_value(backup_value)
            return
        
        if backup_value["service"]:
            sysCommand("systemctl start irqbalance.service", log = True)
            sysCommand("systemctl enable irqbalance.service", log = True)
        else:
            sysCommand("systemctl stop irqbalance.service", log = True)
            sysCommand("systemctl disable irqbalance.service", log = True)

        for interrupt_id, core in backup_value["irqs"].items():
            sysCommand(self.__set_irqs__.format(
                core = core,
                interrupt_id = interrupt_id
            ), log = True)


    def _set_equal_parts_irqs(self, processor_num, partnum=8):
        logger.info(f"[irqs] set portion irqs, processor_num = {processor_num}, portions={partnum}")
        if processor_num < partnum:
            self.__set_range_irqs(
                lb = self.net_info.numa_node_core_range[0],
                ub = self.net_info.numa_node_core_range[1]
            )        

        elif processor_num == partnum or self.net_info.numa_node_core_range[0] == self.net_info.numa_node_core_range[1]:
            self._set_single_irqs(core = self.net_info.numa_node_core_range[1])

        else:
            self.__set_range_irqs(
                lb = self.net_info.numa_node_core_range[1] - int(processor_num/partnum),
                ub = self.net_info.numa_node_core_range[1]
            )
        

    # def _set_eighth_irqs(self, processor_num):
    #     logger.info("[irqs] set eighth irqs, processor_num = {}".format(processor_num))
    #     if processor_num < 8:
    #         self.__set_range_irqs(
    #             lb = self.net_info.numa_node_core_range[0],
    #             ub = self.net_info.numa_node_core_range[1]
    #         )

    #     elif processor_num == 8 or \
    #         self.net_info.numa_node_core_range[0] == self.net_info.numa_node_core_range[1]:
    #         self._set_single_irqs(core = self.net_info.numa_node_core_range[1])

    #     else:
    #         self.__set_range_irqs(
    #             lb = self.net_info.numa_node_core_range[1] - int(processor_num/8),
    #             ub = self.net_info.numa_node_core_range[1]
    #         )


    def __set_range_irqs(self, lb, ub):
        logger.info("[irqs] set range irqs, lb = {}, ub = {}".format(lb, ub))
        for interrupt_id in self.net_info.interrupts:
            sysCommand(self.__set_irqs__.format(
                core = "{}-{}".format(lb, ub),
                interrupt_id = interrupt_id
            ), log = True)


    def _set_single_irqs(self, core):
        logger.info("[irqs] set single irqs, core = {}".format(core))
        for interrupt_id in self.net_info.interrupts:
            sysCommand(self.__set_irqs__.format(
                core = core,
                interrupt_id = interrupt_id
            ), log = True)


    def _set_average_irqs(self, input_numa_node, output_numa_node, reverse=False):
        logger.info("[irqs] set average irqs, input_numa_node = {}, output_numa_node = {}".format(
                    input_numa_node, output_numa_node))

        for index, interrupt_id in enumerate(self.net_info.input_interrupts_list):
            core = input_numa_node[0] + index % (input_numa_node[1] - input_numa_node[0] + 1)
            sysCommand(self.__set_irqs__.format(
                core = core,
                interrupt_id = interrupt_id
            ), log = True)

        if reverse:
            output_interrupts_list = self.net_info.output_interrupts_list[::-1]
        else:
            output_interrupts_list = self.net_info.output_interrupts_list

        for index, interrupt_id in enumerate(output_interrupts_list):
            core = output_numa_node[0] + index % (output_numa_node[1] - output_numa_node[0] + 1)
            sysCommand(self.__set_irqs__.format(
                core = core,
                interrupt_id = interrupt_id
            ), log = True)


    def _set_balance_irqs(self, interrupts_list, processor_num):
        logger.info("[irqs] set balance irqs, processor_num = {}, interrupts = {}".format(
                    processor_num, interrupts_list))
        
        # cpus > irqs
        if processor_num >= len(interrupts_list):
            per_irq_cpus = int(processor_num / len(interrupts_list))
            cpu_id = 0
            for interrupt_id in interrupts_list:
                if per_irq_cpus == 1:
                    sysCommand(self.__set_irqs__.format(
                        core = "{}".format(cpu_id),
                        interrupt_id = interrupt_id
                    ), log = True)
                else:
                    sysCommand(self.__set_irqs__.format(
                        core = "{}-{}".format(cpu_id, cpu_id + per_irq_cpus - 1),
                        interrupt_id = interrupt_id
                    ), log = True)
                cpu_id += per_irq_cpus

        # irqs > cpus
        else:
            per_cpu_irqs = int(len(interrupts_list) / processor_num)
            cpu_id, binding_count = 0, per_cpu_irqs
            for interrupt_id in interrupts_list:
                sysCommand(self.__set_irqs__.format(
                    core = cpu_id,
                    interrupt_id = interrupt_id
                ), log = True)
                binding_count -= 1
                if binding_count == 0:
                    cpu_id, binding_count = cpu_id + 1, per_cpu_irqs


    def _set_service(self, value):
        if value == "off":
            sysCommand("systemctl start irqbalance.service", log = True)
            sysCommand("systemctl enable irqbalance.service", log = True)
        else:
            sysCommand("systemctl stop irqbalance.service", log = True)
            sysCommand("systemctl disable irqbalance.service", log = True)


    def set_value(self, value):
        logger.info("[irqs] set value: {}".format(value))
        if type(value) is dict:
            self._set_backup_value(value)
            return
        
        self._set_service(value)

        # Set numa0 to each interrupts
        if value == "dedicated":
            self._set_single_irqs(core = self.net_info.numa_node_core_range[0])

        elif value in ["numa0"]:
            self._set_average_irqs(
                input_numa_node  = [self.net_info.numa_node_core_range[0], int(self.net_info.numa_node_core_range[1]/2)],
                output_numa_node = [self.net_info.numa_node_core_range[0], int(self.net_info.numa_node_core_range[1]/2)]
            )

        elif value == "numa1":
            self._set_average_irqs(
                input_numa_node  = [min(self.net_info.numa_node_core_range[1], int(self.net_info.numa_node_core_range[1]/2) + 1), self.net_info.numa_node_core_range[1]],
                output_numa_node = [min(self.net_info.numa_node_core_range[1], int(self.net_info.numa_node_core_range[1]/2) + 1), self.net_info.numa_node_core_range[1]]
            )

        # Sequentially allocate core for interrupts
        elif value == "disperse":
            self._set_average_irqs(
                input_numa_node  = self.net_info.numa_node_core_range,
                output_numa_node = self.net_info.numa_node_core_range
            )

        # Base on disperse, distinguishing input interrupts and output interrupts
        elif value == "different":
            self._set_average_irqs(
                input_numa_node  = [self.net_info.numa_node_core_range[0], int(self.net_info.numa_node_core_range[1]/2)],
                output_numa_node = [min(self.net_info.numa_node_core_range[1], int(self.net_info.numa_node_core_range[1]/2) + 1), self.net_info.numa_node_core_range[1]]
            )

        elif value == "opposite":
            self._set_average_irqs(
                input_numa_node  = self.net_info.numa_node_core_range,
                output_numa_node = self.net_info.numa_node_core_range,
                reverse = True
            )

        elif value == "eighth":
            self._set_equal_parts_irqs(
                processor_num = self.net_info.processor, 
                partnum = 8
            )

        elif value == "balance":
            self._set_balance_irqs(
                interrupts_list = self.net_info.interrupts, 
                processor_num = self.net_info.processor)
        
        elif value == "off":
            self.__set_range_irqs(
                lb = self.net_info.numa_node_core_range[0],
                ub = self.net_info.numa_node_core_range[1]
            )
        
        elif re.search(r"(\d*)portions", value):
            self._set_equal_parts_irqs(
                processor_num = self.net_info.processor, 
                partnum = int(re.search(r"(\d*)portions", value).group(1))
            )
        
        return value
