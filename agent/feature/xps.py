# Copyright (c) 2021-2023 Alibaba Cloud Computing Ltd.
# SPDX-License-Identifier: MulanPSL-2.0
# pylint: disable=C,redefined-outer-name,unspecified-encoding,import-error,logging-format-interpolation,broad-except,no-self-use,protected-access,abstract-method,consider-using-with,too-many-instance-attributes,too-many-arguments,eval-used,fixme,attribute-defined-outside-init

import logging

from agent.common.system import sysCommand
from agent.feature.base import Feature
from agent.common.netinfo import NetInfo
from agent.common.tools import padding_code

logger = logging.getLogger('common')

class XPS(Feature):
    __domain__  = "net"
    __feature__ = "XPS"
    
    __get_xps__ = "cat /sys/class/net/{dev_name}/queues/{q_name}/xps_cpus"
    __set_xps__ = "echo {value} > /sys/class/net/{dev_name}/queues/{q_name}/xps_cpus"
    
    def __init__(self) -> None:
        super().__init__()
        self.net_info = NetInfo()


    def _set_backup_value(self, backup_value):
        logger.info("[XPS] Set backup XPS value to rollback: {}".format(backup_value))
        for q_name, value in backup_value.items():
            sysCommand(self.__set_xps__.format(
                value    = value, 
                dev_name = self.net_info.dev_name, 
                q_name   = q_name), log = True)


    def set_value(self, value):
        if type(value) is dict:
            self._set_backup_value(value)
            return
        
        total_cpus  = self.net_info.processor
        queues_num  = len(self.net_info.tx_queue)
        net_dev     = self.net_info.dev_name

        logger.info("[XPS] Set XPS: total cpus = {}, queues num = {}, net dev = {}".format(total_cpus, queues_num, net_dev))

        for q_index, q_name in enumerate(self.net_info.tx_queue):
            if value in ["circulate", "disperse"]:
                bitmap = "".join(['1' if core_index % queues_num == q_index % total_cpus \
                                else '0' for core_index in range(total_cpus)][::-1])
                
            elif value in ["half", "different"]:
                used_cpus = int(total_cpus / 2)
                bitmap = "".join(['1' if core_index % queues_num == q_index % used_cpus \
                                else '0' for core_index in range(used_cpus)][::-1])
                
            elif value == "all":
                bitmap = "".join(['1', ] * total_cpus)
            
            elif value == "off":
                bitmap = "".join(['0', ] * total_cpus)
            
            else:
                # auto setting
                _per_queue_cpus = max(int(total_cpus/queues_num), 1)
                _per_cpu_queues = max(int(queues_num/total_cpus), 1)
                bitmap = "".join(['1' if int(core_index/_per_queue_cpus) == int(q_index/_per_cpu_queues) \
                                else '0' for core_index in range(total_cpus)][::-1])
            
            hex_code = padding_code(hex(int(bitmap,2))[2:], total_cpus)
            sysCommand(self.__set_xps__.format(
                value    = hex_code, 
                dev_name = net_dev, 
                q_name   = q_name), log = True)
        return value


    def get_value(self):
        value = {}
        for q_name in self.net_info.tx_queue:
            value[q_name] = sysCommand(self.__get_xps__.format(
                dev_name = self.net_info.dev_name, 
                q_name   = q_name))
        return value
