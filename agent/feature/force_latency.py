# Copyright (c) 2021-2023 Alibaba Cloud Computing Ltd.
# SPDX-License-Identifier: MulanPSL-2.0
# pylint: disable=C,redefined-outer-name,unspecified-encoding,import-error,logging-format-interpolation,broad-except,no-self-use,protected-access,abstract-method,consider-using-with,too-many-instance-attributes,too-many-arguments,eval-used,fixme,attribute-defined-outside-init
import os
import logging
import struct

from agent.feature.base import Feature
from agent.common.exception import InitException, DomainBasicException
from agent.common.system import sysCommand
from agent.common.config import ConfigFile


logger = logging.getLogger('common')


class ForceLatency(Feature):
    __domain__  = "cpu"
    __feature__ = "force_latency"
    __cpu_dma_latency__ = ConfigFile.conf['cpu']['DMA_LATENCY']
    __cpuidle_states__  = ConfigFile.conf['cpu']['CPUIDLE']

    def __init__(self) -> None:
        """ Check available of this feature
        """
        super().__init__()
        if not os.path.exists(self.__cpu_dma_latency__):
            raise InitException(
                "file {} not exists".format(self.__cpu_dma_latency__), 
                self.__domain__, self.__feature__)

        if not os.path.exists(self.__cpuidle_states__):
            raise InitException(
                "file {} not exists".format(self.__cpuidle_states__), 
                self.__domain__, self.__feature__)
        
        self._cpu_latency_fd = None
    

    def _get_latency_by_cstate_id(self, lid, no_zero = False):
        latency_path = os.path.join(self.__cpuidle_states__, "state{}".format(lid), "latency")
        if not os.path.exists(latency_path):
            raise DomainBasicException(
                "state file not exists: {}".format(latency_path), 
                self.__domain__, self.__feature__)
        
        latency = int(sysCommand("cat {}".format(latency_path)))
        if no_zero and latency == 0:
            logger.warning("skipping zero latency")
            return None
        
        return latency


    def _parse_latency(self, latency):
        self.cstates_latency = None
        for _latency in str(latency).split("|"):
            if _latency.isdigit():
                return int(_latency)
            if _latency[0:18] == "cstate.id_no_zero:":
                return self._get_latency_by_cstate_id(_latency[18:], no_zero = True)
            if _latency[0:10] == "cstate.id:":
                return self._get_latency_by_cstate_id(_latency[10:])
        raise DomainBasicException("invalid latency {}".format(latency), self.__domain__, self.__feature__)


    def set_value(self, value):
        logger.info("[DMA] open file {}".format(self.__cpu_dma_latency__))
        self._cpu_latency_fd = os.open(self.__cpu_dma_latency__, os.O_WRONLY)

        latency = self._parse_latency(str(value))
        latency_bin = struct.pack("i",latency)

        logger.info("[DMA] set latency value '{}'".format(latency_bin))
        os.write(self._cpu_latency_fd, latency_bin)
        return value


    def get_value(self):
        """ No need to backup parameter values

        """
        return None


    def rollback(self):
        """ Close file to rollback value

        """
        if self._cpu_latency_fd is None:
            return
        
        try:
            os.close(self._cpu_latency_fd)
            self._cpu_latency_fd = None
            logger.info("[DMA] close file to rollback")
        
        except OSError as e:
            logger.error(f"fail to close file '{self.__cpu_dma_latency__}': {e}")
            raise
