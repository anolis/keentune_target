# Copyright (c) 2021-2023 Alibaba Cloud Computing Ltd.
# SPDX-License-Identifier: MulanPSL-2.0
# pylint: disable=C,redefined-outer-name,unspecified-encoding,import-error,logging-format-interpolation,broad-except,no-self-use,protected-access,abstract-method,consider-using-with,too-many-instance-attributes,too-many-arguments,eval-used,fixme,attribute-defined-outside-init

import re
import logging

from agent.feature.base import Feature
from agent.common.system import sysCommand
from agent.common.config import ConfigFile


logger = logging.getLogger("common")


class SchedFeature(Feature):
    __domain__  = "kernel_cpu"
    __feature__ = "sched_features"
    __shced_features__ = ConfigFile.conf['sched_features']['CONFIG_FILE']

    def __init__(self):
        super().__init__()
        # check authority
        sysCommand("cat {}".format(self.__shced_features__))

    def set_value(self, value):
        for v in re.split(r"[\s,;]", value):
            if v == "":
                continue
            assert re.match(r"^[A-Z_]+$",v)
            sysCommand("echo {} > {}".format(v, self.__shced_features__), log = True)
        return value
    
    def get_value(self):
        value = sysCommand("cat {}".format(self.__shced_features__))
        return value
