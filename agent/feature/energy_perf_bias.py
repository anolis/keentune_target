# Copyright (c) 2021-2023 Alibaba Cloud Computing Ltd.
# SPDX-License-Identifier: MulanPSL-2.0
# pylint: disable=C,redefined-outer-name,unspecified-encoding,import-error,logging-format-interpolation,broad-except,no-self-use,protected-access,abstract-method,consider-using-with,too-many-instance-attributes,too-many-arguments,eval-used,fixme,attribute-defined-outside-init
import re
import logging

from agent.common.system import sysCommand
from agent.feature.base import Feature
from agent.common.exception import InitException

logger = logging.getLogger('common')

class EnergyPerfBias(Feature):
    __domain__    = "cpu"
    __feature__   = "energy_perf_bias"

    def __init__(self) -> None:
        """ Check available of this feature
        """
        super().__init__()
        try:
            sysCommand("x86_energy_perf_policy normal")
        except Exception as e:
            raise InitException("not enabled on this platform", self.__domain__, self.__feature__) from e
        

    def set_value(self, value):
        """ Setting parameter value
        Args:
            value (string): param value

        Raises:
            Exception: Setting value failed
        """
        if re.search(r"\|", value):
            for v in re.split(r"\|", value):
                try:
                    assert v in ['performance', 'normal', 'powersave']
                    sysCommand(f"x86_energy_perf_policy {v}", log = True)
                    return v
                
                except Exception:
                    continue
            
            raise Exception("All optional value {} setting error".format(value))
        
        else:
            assert value in ['performance', 'normal', 'powersave']
            sysCommand(f"x86_energy_perf_policy {value}", log = True)
            return value


    def get_value(self):
        return "normal"
