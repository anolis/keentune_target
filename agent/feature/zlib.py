# Copyright (c) 2021-2023 Alibaba Cloud Computing Ltd.
# SPDX-License-Identifier: MulanPSL-2.0
# pylint: disable=C,redefined-outer-name,unspecified-encoding,import-error,logging-format-interpolation,broad-except,no-self-use,protected-access,abstract-method,consider-using-with,too-many-instance-attributes,too-many-arguments,eval-used,fixme,attribute-defined-outside-init

import os
import logging

from agent.feature.base import Feature
from agent.common.system import sysCommand
from agent.common.config import Config
from agent.common.exception import InitException

logger = logging.getLogger('common')

class Zlib(Feature):
    __domain__  = "net"
    __feature__ = "zlib_optimized"
    __lib_name__= "zlib.al8.aarch64.rpm"
    
    def __init__(self) -> None:
        super().__init__()
        # * check if gzip feature is available in this vm
        arch = sysCommand("arch")
        if arch != "aarch64":
            raise InitException("unavailable in arch '{}'".format(arch), self.__domain__, self.__feature__)
        
        if not os.path.exists(os.path.join(Config.LIB_PATH, self.__lib_name__)):
            raise InitException("lib file do not exists: {}".format(
                        os.path.join(Config.LIB_PATH, self.__lib_name__)), self.__domain__, self.__feature__)

        self.rpm_name = sysCommand("rpm -q {}".format(os.path.join(Config.LIB_PATH, self.__lib_name__)))
        
    
    def set_value(self, value):
        assert value in ['on', 'off']
        if value == self.get_value():
            return
        
        if value == "on":
            sysCommand("rpm -ivh {} --force".format(self.__lib_name__), 
                       cwd = Config.LIB_PATH, log = True)
        else:
            sysCommand("rpm -e {}".format(self.rpm_name), 
                       cwd = Config.LIB_PATH, log = True)
        return value


    def get_value(self):
        # * return current feature value 'on' or 'off'
        try:
            return "on" if sysCommand(
                "rpm -qa | grep {}".format(self.rpm_name
            )) != "" else "off"
        
        except Exception:
            return "off"
