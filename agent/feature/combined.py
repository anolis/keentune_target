import re
import logging

from agent.feature.base import Feature
from agent.common.netinfo import NetInfo
from agent.common.system import sysCommand


logger = logging.getLogger("common")


class Combined(Feature):
    __domain__  = "net"
    __feature__ = "combined"

    def __init__(self) -> None:
        super().__init__()
        self.net_info = NetInfo()
        info = sysCommand(f"ethtool -l {self.net_info.dev_name}")
        self.max_combined, _ = re.findall(r"Combined:\s+(\d+)", info)
        self.max_combined = int(self.max_combined)


    def adjust_by_max(self, value):
        value = int(value)
        if value > self.max_combined:
            logger.info(f"{value} exceeded combined pre-set maximum: {self.max_combined}, set net queue combined to max.")
            return self.max_combined
        else:
            logger.info(f"set net queue combined to {value}")
            return value


    def get_value(self):
        info = sysCommand(f"ethtool -l {self.net_info.dev_name}")
        _, value = re.findall(r"Combined:\s+(\d+)", info)
        return int(value)


    def set_value(self, value):
        if str(value).isdigit():
            combined_value = self.adjust_by_max(value)

        elif value == "max":
            combined_value = self.max_combined

        elif value == "half":
            combined_value = self.adjust_by_max(self.net_info.processor/2)
        
        elif value == "quarter":
            combined_value = self.adjust_by_max(self.net_info.processor/4)

        elif value == "eighth":
            combined_value = self.adjust_by_max(self.net_info.processor/8)
        
        else:
            raise Exception(f"invaild value: {value}")
        
        logger.info(f"set net queue combined to {combined_value}")
        sysCommand(f"ethtool -L {self.net_info.dev_name} combined {combined_value}")
