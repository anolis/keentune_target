# Copyright (c) 2021-2023 Alibaba Cloud Computing Ltd.
# SPDX-License-Identifier: MulanPSL-2.0
# pylint: disable=C,redefined-outer-name,unspecified-encoding,import-error,logging-format-interpolation,broad-except,no-self-use,protected-access,abstract-method,consider-using-with,too-many-instance-attributes,too-many-arguments,eval-used,fixme,attribute-defined-outside-init

import os
import json
import logging

from abc import ABCMeta, abstractmethod
from agent.common.config import Config

logger = logging.getLogger('common')

""" Feature basic class
"""

class Feature(metaclass=ABCMeta):
    __domain__  = "base"
    __feature__ = "base"
    
    @classmethod
    def isActived(self):
        """ Loading Check,  If backup file exists, this domain is actived.

        """
        backup_file = self.backup_file_path()
        if os.path.exists(backup_file):
            return True
    
    
    @classmethod
    def backup_file_path(self):
        """ Return backup file path
        
        Function 1. Backup: Save backup value in this path
        Function 2. Loading Check: If backup file exists, load domain to rollback when keentune-target started.
        """
        return os.path.join(Config.BACKUP_PATH, "{}_{}.json".format(self.__domain__, self.__feature__))


    @abstractmethod
    def set_value(self, value):
        """ value setting method of this feature

        Raises:
            Exception: value setting error
        """
    

    @abstractmethod
    def get_value(self):        
        """ value reading method of this feature

        Raises:
            Exception: value reading error
        """


    def backup(self):
        backup_value = self.get_value()
        if backup_value is None:
            return
        with open(self.backup_file_path(), "w") as f:
            f.write(json.dumps(backup_value))


    def rollback(self):
        if not os.path.exists(self.backup_file_path()):
            return
    
        with open(self.backup_file_path(), "r") as f:
            value = json.load(f)

        self.set_value(value) #! raise exception
        logger.info(f"'{self.__domain__}:{self.__feature__}' rollback success, remove bakcup file")
        os.remove(self.backup_file_path())
