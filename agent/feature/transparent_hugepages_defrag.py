# Copyright (c) 2021-2023 Alibaba Cloud Computing Ltd.
# SPDX-License-Identifier: MulanPSL-2.0
# pylint: disable=C,redefined-outer-name,unspecified-encoding,import-error,logging-format-interpolation,broad-except,no-self-use,protected-access,abstract-method,consider-using-with,too-many-instance-attributes,too-many-arguments,eval-used,fixme,attribute-defined-outside-init
import re
import logging

from agent.feature.base import Feature
from agent.common.system import sysCommand
from agent.common.exception import DomainBasicException
from agent.common.config import ConfigFile


logger = logging.getLogger('common')


class Hugepages(Feature):
    __domain__ = "vm" 
    __feature__ = "transparent_hugepages_defrag"
    __config_file__ = ConfigFile.conf['vm']['HUGEPAGES_DEFRAG']
    
    def set_value(self, value):
        if value not in ["always", "defer", "defer+madvise", "madvise", "never"]:
            raise DomainBasicException("invalid value: {}".format(value), self.__domain__, self.__feature__)
        
        sysCommand("echo {} > {}".format(value, self.__config_file__), log = True)
        return value

    def get_value(self):
        options = sysCommand("cat {}".format(self.__config_file__))
        _match = re.match(r'.*\[([^\]]+)\].*', options)
        if _match:
            return _match.group(1)
        else:
            raise DomainBasicException("Can't parse file content: {}".format(options), self.__domain__, self.__feature__)
