# Copyright (c) 2021-2023 Alibaba Cloud Computing Ltd.
# SPDX-License-Identifier: MulanPSL-2.0
# pylint: disable=C,redefined-outer-name,unspecified-encoding,import-error,logging-format-interpolation,broad-except,no-self-use,protected-access,abstract-method,consider-using-with,too-many-instance-attributes,too-many-arguments,eval-used,fixme,attribute-defined-outside-init

import logging

from agent.common.system import sysCommand
from agent.feature.base import Feature
from agent.common.netinfo import NetInfo
from agent.common.tools import padding_code

logger = logging.getLogger('common')

class RPS(Feature):
    __domain__  = "net"
    __feature__ = "RPS"

    __get_rps__ = "cat /sys/class/net/{dev_name}/queues/{q_name}/rps_cpus"
    __get_rfs__ = "cat /sys/class/net/{dev_name}/queues/{q_name}/rps_flow_cnt"
    __set_rps__ = "echo {value} > /sys/class/net/{dev_name}/queues/{q_name}/rps_cpus"
    __set_rfs__ = "echo {value} > /sys/class/net/{dev_name}/queues/{q_name}/rps_flow_cnt"
    __get_flow_entries__ = "sysctl -n net.core.rps_sock_flow_entries"
    __set_flow_entries__ = "sysctl -w net.core.rps_sock_flow_entries={value}"
    
    def __init__(self):
        super().__init__()
        self.net_info = NetInfo()


    def _set_old_version_backup_value(self, backup_value):
        """
        Compatible with versions 2.4.0 and earlier which save backupfile as {'rx-0': '00'}

        """
        logger.info("set backup value by old version")
        for q_name, value in backup_value.items():
            sysCommand(self.__set_rps__.format(
                        value    = value,
                        dev_name = self.net_info.dev_name, 
                        q_name   = q_name), log = True)


    def _set_backup_value(self, backup_value):
        logger.info("[RPS] Set backup RPS/RFS value to rollback: {}".format(backup_value))
        if not backup_value.__contains__('rps'):
            self._set_old_version_backup_value(backup_value)
            return
        
        for q_name, value in backup_value['rps'].items():
            sysCommand(self.__set_rps__.format(
                        value    = value,
                        dev_name = self.net_info.dev_name, 
                        q_name   = q_name), log = True)

        for q_name, value in backup_value['rfs'].items():
            sysCommand(self.__set_rfs__.format(
                        value    = value,
                        dev_name = self.net_info.dev_name, 
                        q_name   = q_name), log = True)
        
        sysCommand(self.__set_flow_entries__.format(value = backup_value['rps_sock_flow_entries']), log = True)


    def set_value(self, value):
        if type(value) is dict:
            self._set_backup_value(value)
            return
        
        total_cpus  = self.net_info.processor
        queues_num  = len(self.net_info.rx_queue)
        net_dev     = self.net_info.dev_name

        logger.info("[RPS] Set RPS/RFS: total cpus = {}, queues num = {}, net dev = {}".format(total_cpus, queues_num, net_dev))

        for q_index, q_name in enumerate(self.net_info.rx_queue):
            if value in ["circulate", "disperse"]:
                bitmap = "".join(['1' if core_index % queues_num == q_index % total_cpus \
                                else '0' for core_index in range(total_cpus)][::-1])
                
            elif value in ["half", "different"]:
                used_cpus = int(total_cpus / 2)
                bitmap = "".join(['1' if core_index % queues_num == q_index % used_cpus \
                                else '0' for core_index in range(used_cpus)][::-1])
                hex_code = padding_code(hex(int(bitmap,2))[2:], total_cpus, used_cpus=used_cpus)

            elif value == "off":
                bitmap = "".join(['0', ] * total_cpus)
                hex_code = padding_code(hex(int(bitmap,2))[2:], total_cpus)

            else:
                # auto setting
                bitmap = "".join(['1', ] * total_cpus)
                hex_code = padding_code(hex(int(bitmap,2))[2:], total_cpus)

            sysCommand(self.__set_rps__.format(
                    value    = hex_code, 
                    dev_name = net_dev, 
                    q_name   = q_name), log = True)
            
            if value != "off":
                sysCommand(self.__set_rfs__.format(
                    value    = 4096,
                    dev_name = net_dev, 
                    q_name   = q_name), log = True)

        if value != "off":
            sysCommand(self.__set_flow_entries__.format(value = 4096 * total_cpus),log = True)
        return value


    def get_value(self):
        """ Read rps, rfs and net.core.rps_sock_flow_entries value to backup

        """
        self.rps_value = {}
        for q_name in self.net_info.rx_queue:
            self.rps_value[q_name] = sysCommand(self.__get_rps__.format(dev_name = self.net_info.dev_name, q_name = q_name))
        
        self.rfs_value = {}
        for q_name in self.net_info.rx_queue:
            self.rfs_value[q_name] = sysCommand(self.__get_rfs__.format(dev_name = self.net_info.dev_name, q_name = q_name))
        
        self.rps_sock_flow_entries = sysCommand(self.__get_flow_entries__)

        value = {
            "rps": self.rps_value,
            "rfs": self.rfs_value,
            "rps_sock_flow_entries": self.rps_sock_flow_entries
        }
        return value
