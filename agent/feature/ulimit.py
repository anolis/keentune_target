# Copyright (c) 2021-2023 Alibaba Cloud Computing Ltd.
# SPDX-License-Identifier: MulanPSL-2.0
# pylint: disable=C,redefined-outer-name,unspecified-encoding,import-error,logging-format-interpolation,broad-except,no-self-use,protected-access,abstract-method,consider-using-with,too-many-instance-attributes,too-many-arguments,eval-used,fixme,attribute-defined-outside-init

from agent.common.system import sysCommand
from agent.feature.base import Feature

class Ulimit(Feature):
    __domain__  = "limits"
    __feature__ = "ulimit"
    __set_cmd__ = "ulimit -n {value}"
    __get_cmd__ = "ulimit -n"

    def set_value(self, value):
        assert str(value).isdigit()
        
        sysCommand(self.__set_cmd__.format(
            value = value), log = True)
        return value
    
    
    def get_value(self):
        return sysCommand(self.__get_cmd__)
