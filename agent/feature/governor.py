# Copyright (c) 2021-2023 Alibaba Cloud Computing Ltd.
# SPDX-License-Identifier: MulanPSL-2.0
# pylint: disable=C,redefined-outer-name,unspecified-encoding,import-error,logging-format-interpolation,broad-except,no-self-use,protected-access,abstract-method,consider-using-with,too-many-instance-attributes,too-many-arguments,eval-used,fixme,attribute-defined-outside-init

import re
import os
import logging

from agent.common.system import sysCommand
from agent.feature.base import Feature
from agent.common.exception import InitException

logger = logging.getLogger('common')

class Governor(Feature):
    __domain__ = "cpu"
    __feature__ = "governor"
    __set_governor__ = "cpupower -c {cpu_index} frequency-set -g {governor}"
    __get_governor__ = "cat /sys/devices/system/cpu/cpu{cpu_index}/cpufreq/scaling_governor"
    __available_governor__ = ['performance', 'powersave', 'ondemand', 'userspace', 'conservative']

    def __init__(self) -> None:
        """ Check available of this feature
        """
        super().__init__()
        try:
            sysCommand("which cpupower")
        except Exception:
            raise InitException("governor control unavailable", self.__domain__, self.__feature__)
        
        governor_info = sysCommand("cpupower -c all frequency-info")
        cpu_governor_info_list = [i for i in re.split(r"analyzing CPU \d+:", governor_info) if i!=""]

        self.cpu_available_governor = {}
        governor_avaliable = False

        for index, cpu_governor_info in enumerate(cpu_governor_info_list):
            available_governor_match = re.search(r"available cpufreq governors:(.*)", cpu_governor_info)
            
            if available_governor_match is None:
                logger.warning("[Governor] CPU{} no available cpufreq governors".format(index))
                continue

            available_governor = [i.strip() for i in available_governor_match.group(1).split(' ') if i in self.__available_governor__]
            self.cpu_available_governor[index] = available_governor
            if len(available_governor) > 0:
                governor_avaliable = True
            
        if not governor_avaliable:
            logger.warning("[Governor] no CPU governor available, disable governor setting")
            raise InitException("no CPU governor available", self.__domain__, self.__feature__)
    

    def _check_cpu_can_change_governor(self, cpu_index):
        cpu_online = sysCommand("cat /sys/devices/system/cpu/cpu{cpu_index}/online".format(cpu_index = cpu_index)).strip()
        if cpu_online != "1":
            logger.warning("[Governor] CPU{} offline".format(cpu_index))
            return False
        
        if not os.path.exists("/sys/devices/system/cpu/cpu{cpu_index}/cpufreq/scaling_governor".format(cpu_index = cpu_index)):
            logger.warning("[Governor] CPU{} has no scaling governor".format(cpu_index))
            return False

        return True


    def _parseValidGovernor(self, governor, available_list):
        """ select valid governor
        
        e.g. ondemand|powersave -> powersave, if powersave in available_list
        """
        governor_list = governor.split('|')
        for _gov in governor_list:
            if _gov in available_list:
                return _gov
        return None


    def _set_backup_value(self, backup_value):
        for cpu_index, governor in backup_value.items():
            sysCommand(self.__set_governor__.format(
                cpu_index = cpu_index,
                governor  = governor
            ), log = True)


    def set_value(self, value):
        """ Setting parameter value

        """
        if type(value) is dict:
            self._set_backup_value(value)
            return
        
        for cpu_index in self.cpu_available_governor.keys():
            if not self._check_cpu_can_change_governor(cpu_index):
                continue
            
            for governor in value.split('|'):
                if governor in self.cpu_available_governor[cpu_index]:
                    sysCommand(self.__set_governor__.format(
                        cpu_index = cpu_index,
                        governor  = governor), log = True)
                    return governor


    def get_value(self):
        """ Reading parameter values in the current environment.
        
        """
        govergor = {}
        for cpu_index in self.cpu_available_governor.keys():
            if not self._check_cpu_can_change_governor(cpu_index):
                continue
            govergor[cpu_index] = sysCommand(self.__get_governor__.format(cpu_index = cpu_index)).strip()
        return govergor
