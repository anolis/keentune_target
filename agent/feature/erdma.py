# Copyright (c) 2021-2023 Alibaba Cloud Computing Ltd.
# SPDX-License-Identifier: MulanPSL-2.0
# pylint: disable=C,redefined-outer-name,unspecified-encoding,import-error,logging-format-interpolation,broad-except,no-self-use,protected-access,abstract-method,consider-using-with,too-many-instance-attributes,too-many-arguments,eval-used,fixme,attribute-defined-outside-init

import os
import logging

from agent.common.system import sysCommand
from agent.feature.base import Feature
from agent.common.config import Config
from agent.common.exception import InitException

logger = logging.getLogger('common')

class ERDMA(Feature):
    __domain__ = "net"
    __feature__ = "erdma"
    
    def __init__(self) -> None:
        """ Check available of this feature, 
        
        Raises:
            Exception: feature file do not exists
        """
        super().__init__()

        self.erdma_script = os.path.join(Config.SCRIPTS_PATH, "erdma.sh")
        assert os.path.exists(self.erdma_script)
        
        try:
            logger.info("[eRDMA] check eRDMA driver")
            sysCommand(f"{self.erdma_script} hasERDMA", log = True)
        except Exception:
            raise InitException("eRDMA driver unavailable", self.__domain__, self.__feature__)
    

    def set_value(self, value):
        """ set feature value

        Args:
            value (_type_): _description_
        """
        assert value in ['on', 'off']
        sysCommand(f"{self.erdma_script} toggleERDMA {value}", log = True)
        return value
    

    def get_value(self):
        """ read current parameter value

        Returns:
            _type_: _description_
        """
        return sysCommand(f"{self.erdma_script} isERDMA")
