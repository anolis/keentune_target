# Copyright (c) 2021-2023 Alibaba Cloud Computing Ltd.
# SPDX-License-Identifier: MulanPSL-2.0
# pylint: disable=C,redefined-outer-name,unspecified-encoding,import-error,logging-format-interpolation,broad-except,no-self-use,protected-access,abstract-method,consider-using-with,too-many-instance-attributes,too-many-arguments,eval-used,fixme,attribute-defined-outside-init

import logging

from agent.common.tools import glob
from agent.feature.base import Feature
from agent.common.system import sysCommand
from agent.common.exception import InitException

logger = logging.getLogger('common')

class Alpm(Feature):
    __domain__  = "scsi_host"
    __feature__ = "alpm"
    
    def __init__(self) -> None:
        super().__init__()
        self.scsi_paths = glob("/sys/class/scsi_host/host*/link_power_management_policy")
        logger.info("[alpm] scsi host path: {}".format(self.scsi_paths))
        if self.scsi_paths.__len__() == 0:
            raise InitException("Can not find any hosts in /sys/class/scsi_host", self.__domain__, self.__feature__)
    

    def _set_backup_value(self, backup_value):
        """ Set backup value in dict

        """
        for filepath, value in backup_value.items():
            sysCommand("echo {} > {}".format(value, filepath), log = True)


    def set_value(self, value):
        """ Set same value to all hosts file

        """
        if type(value) is dict:
            self._set_backup_value(value)
            return
        
        assert value in ['max_performance', 'min_power', 'medium_power']
        
        for filepath in self.scsi_paths:
            sysCommand("echo {} > {}".format(value, filepath), log = True)
        return value


    def get_value(self):
        """ Read dict values for backup

        """
        values = {}
        for filepath in self.scsi_paths:
            value = sysCommand("cat {}".format(filepath))
            values[filepath] = value
        return values
