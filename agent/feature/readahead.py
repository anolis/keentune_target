# Copyright (c) 2021-2023 Alibaba Cloud Computing Ltd.
# SPDX-License-Identifier: MulanPSL-2.0
# pylint: disable=C,redefined-outer-name,unspecified-encoding,import-error,logging-format-interpolation,broad-except,no-self-use,protected-access,abstract-method,consider-using-with,too-many-instance-attributes,too-many-arguments,eval-used,fixme,attribute-defined-outside-init

import logging

from agent.feature.base import Feature
from agent.common.tools import glob
from agent.common.system import sysCommand

logger = logging.getLogger('common')

class Readahead(Feature):
    __domain__ = "disk" 
    __feature__ = "readahead"
    __read_ahead_file__ = "/sys/block/*/queue/read_ahead_kb"

    def __init__(self):
        super().__init__()
        self.file_list = glob(self.__read_ahead_file__)
        logger.info("[readahead] readahead files: {}".format(self.file_list))
    

    def _set_backup_value(self, backup_value):
        for path, value in backup_value.items():
            sysCommand("echo {} > {}".format(value, path), log = True)


    def set_value(self, value):
        if type(value) is dict:
            self._set_backup_value(value)
            return
        
        if type(value) is list:
            #! Compatible with versions 2.4.0 and earlier which save backupfile as [4096]
            value = value[0]
        
        value = str(value)
        if value.startswith(">"):
            value = value[1:]
        
        assert str(value).isdigit()
        for path in self.file_list:
            sysCommand("echo {} > {}".format(value, path), log = True)
        return value


    def get_value(self):
        values = {}
        for path in self.file_list:
            values[path] = sysCommand("cat {}".format(path))
        return values
