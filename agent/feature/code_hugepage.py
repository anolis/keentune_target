# Copyright (c) 2021-2023 Alibaba Cloud Computing Ltd.
# SPDX-License-Identifier: MulanPSL-2.0
# pylint: disable=C,redefined-outer-name,unspecified-encoding,import-error,logging-format-interpolation,broad-except,no-self-use,protected-access,abstract-method,consider-using-with,too-many-instance-attributes,too-many-arguments,eval-used,fixme,attribute-defined-outside-init

import os
import logging

from agent.common.system import sysCommand
from agent.feature.base import Feature
from agent.common.exception import InitException
from agent.common.config import ConfigFile


logger = logging.getLogger('common')


class Hugepage(Feature):
    __domain__ = "kernel_mem"
    __feature__ = "code_hugepage"
    __hugepage_file__ = ConfigFile.conf['vm']['HUGEPAGES_TEXT']

    def __init__(self) -> None:
        """ Check available of this feature, 
        
        Raise exception if file /sys/kernel/mm/transparent_hugepage/hugetext_enabled do not exists

        Raises:
            Exception: feature file do not exists
        """
        super().__init__()
        if not os.path.exists(self.__hugepage_file__):
            raise InitException("file {} don't exists.".format(self.__hugepage_file__), self.__domain__, self.__feature__)
    

    def set_value(self, value):
        assert str(value) in ['0', '1', '2', '3']
        logger.debug("set feature code_hugepage to '{}'".format(value))
        
        if int(value) == 0:
            sysCommand("echo 1 > /sys/kernel/debug/split_huge_pages", log = True)
            sysCommand("echo 3 > /proc/sys/vm/drop_caches", log = True)
        
        sysCommand("echo {param_value} > {hugepage_file}".format(
            param_value = value,
            hugepage_file = self.__hugepage_file__
        ), log = True)
        return value
    

    def get_value(self):
        return sysCommand("cat {hugepage_file}".format(
            hugepage_file = self.__hugepage_file__
        ))
