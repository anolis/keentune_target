# Copyright (c) 2021-2023 Alibaba Cloud Computing Ltd.
# SPDX-License-Identifier: MulanPSL-2.0
# pylint: disable=C,redefined-outer-name,unspecified-encoding,import-error,logging-format-interpolation,broad-except,no-self-use,protected-access,abstract-method,consider-using-with,too-many-instance-attributes,too-many-arguments,eval-used,fixme,attribute-defined-outside-init,anomalous-backslash-in-string
import os
import logging

from agent.common.system import sysCommand
from agent.common.config import Config
from agent.feature.base import Feature
from agent.common.exception import InitException

logger = logging.getLogger('common')

class x264_265(Feature):
    __domain__   = "codec"
    __feature__  = "x264_265"
    __lib_name__ = "ffmpeg-booster-latest.al8.aarch64.rpm"
    __install__  = "rpm -ivh {lib} \n\
                    echo '/usr/local/lib/' >> /etc/ld.so.conf \n\
                    ldconfig"
    __uninstall__= "rpm -e ffmpeg-booster \n\
                    sed -i  '/\/usr\/local\/lib/d'  /etc/ld.so.conf \n\
                    ldconfig"

    def __init__(self) -> None:
        """ Check available of this feature

        Check rpm file, script file and service availability
        Raise an exception to indicate that the feature is not available
        e.g.
            raise Exception("lib file do not exists")
            raise Exception("feature available on ARM only")
                
        Raises:
            Exception: feature is not available
        """
        super().__init__()

        arch = sysCommand("arch")
        if arch != "aarch64":
            raise InitException("unavailable in arch '{}'".format(arch), self.__domain__, self.__feature__)

        self.__lib_path__ = os.path.join(Config.LIB_PATH, self.__lib_name__)
        if not os.path.exists(self.__lib_path__):
            raise InitException("lib file do not exists: {}".format(self.__lib_path__), self.__domain__, self.__feature__)


    def set_value(self, value):
        """ Setting parameter value

        Parameter value is defined in profile file, such as "on"/"off" or other legal string

        Setting value by system command or running a script and 
        raise an exception to indicate that value setting failed

                Args:
            value (string): param value

        Raises:
            Exception: Setting value failed
        """
        if value == "on":
            sysCommand(self.__install__.format(lib=self.__lib_path__),log=True)
        else:
            sysCommand(self.__uninstall__,log=True)
        return value


    def get_value(self):
        """ Reading parameter value in env

        Reading parameter values in the current environment.
        
        Attention:
        1. The parameter value returned must be within the legal range of the parameter and can
            be re-setted by function set_value()
        
        2. If an exception is thrown, it indicates that parameter reading has failed, 
            causing parameter backup to fail. And this parameter will be skipped during setting

        Returns:
            string: parameter value

        Raises:
            Exception: Reading value failed
        """
        try:
            sysCommand("rpm -q ffmpeg-booster")
        except Exception:
            return "off"
        else:
            return "on"
