# Copyright (c) 2021-2023 Alibaba Cloud Computing Ltd.
# SPDX-License-Identifier: MulanPSL-2.0
# pylint: disable=C,redefined-outer-name,unspecified-encoding,import-error,logging-format-interpolation,broad-except,no-self-use,protected-access,abstract-method,consider-using-with,too-many-instance-attributes,too-many-arguments,eval-used,fixme,attribute-defined-outside-init
import os
import tornado
import logging
import signal

from agent.common import config
from agent.controller import status, configure, rollback
from agent.controller import Context

logger  = logging.getLogger('common')

context = Context() #* load domain and feature class

def _exit(signum, _):
    logger.info("keentune-target exit: SIGNUM={}".format(signum))
    if not config.Config.KEEP_CONFIG:
        logger.info("[+] rollback before exit keentune-target")
        context.rollback()
    os._exit(0)


def main():
    signal.signal(signal.SIGTERM, _exit)   # kill pid
    signal.signal(signal.SIGINT, _exit)    # ctrl -

    app = tornado.web.Application(handlers=[
        (r"/configure", configure.ConfigureHandler),
        (r"/rollback",  rollback.RollbackHandler),
        (r"/available", status.AvailableDomainHandler),
        (r"/status",    status.StatusHandler),
        (r"/virt",      status.isVirtualHandler),
        (r"/os_release",status.OSReleaseHandler)
    ])

    if config.Config.ONLY_LOCALHOST:
        app.listen(config.Config.TARGET_PORT, "127.0.0.1")
    else:
        app.listen(config.Config.TARGET_PORT, "0.0.0.0")

    tornado.ioloop.IOLoop.current().start()